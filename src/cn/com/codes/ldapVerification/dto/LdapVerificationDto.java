package cn.com.codes.ldapVerification.dto;

import java.util.Hashtable;

import javax.naming.ldap.Control;

import cn.com.codes.framework.transmission.dto.BaseDto;
import cn.com.codes.object.LdapConfigInfo;
import cn.com.codes.object.User;

public class LdapVerificationDto extends BaseDto {

	private static final long serialVersionUID = 1L;
	
	private User user;
	private Hashtable<String, String> hashtable;
	private Control[] controls;
	private String url;
	private String baseDN;
	private LdapConfigInfo ldapConfigInfo;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Hashtable<String, String> getHashtable() {
		return hashtable;
	}

	public void setHashtable(Hashtable<String, String> hashtable) {
		this.hashtable = hashtable;
	}

	public Control[] getControls() {
		return controls;
	}

	public void setControls(Control[] controls) {
		this.controls = controls;
	}

	public String getBaseDN() {
		return baseDN;
	}

	public void setBaseDN(String baseDN) {
		this.baseDN = baseDN;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public LdapConfigInfo getLdapConfigInfo() {
		return ldapConfigInfo;
	}

	public void setLdapConfigInfo(LdapConfigInfo ldapConfigInfo) {
		this.ldapConfigInfo = ldapConfigInfo;
	}
	
}
