package cn.com.codes.ldapVerification.web;

import cn.com.codes.framework.app.blh.BaseBizLogicHandler;
import cn.com.codes.framework.exception.BaseException;
import cn.com.codes.framework.transmission.events.BusiRequestEvent;
import cn.com.codes.framework.web.action.BaseAction;
import cn.com.codes.ldapVerification.blh.LdapVerificationBlh;
import cn.com.codes.ldapVerification.dto.LdapVerificationDto;

public class LdapVerificationAction extends BaseAction<LdapVerificationBlh> {

	private static final long serialVersionUID = 1L;
	private LdapVerificationDto dto = new LdapVerificationDto();
	private LdapVerificationBlh ldapVerificationBlh;		 
	

	@Override
	protected void _prepareRequest(BusiRequestEvent reqEvent)
			throws BaseException {
		reqEvent.setDto(dto);
	}

	public LdapVerificationDto getDto() {
		return dto;
	}


	public void setDto(LdapVerificationDto dto) {
		this.dto = dto;
	}


	public LdapVerificationBlh getLdapVerificationBlh() {
		return ldapVerificationBlh;
	}



	public void setLdapVerificationBlh(LdapVerificationBlh ldapVerificationBlh) {
		this.ldapVerificationBlh = ldapVerificationBlh;
	}
	
	public  BaseBizLogicHandler getBlh(){
		return ldapVerificationBlh;
	}

}
