package cn.com.codes.ldapVerification.service.impl;

import java.util.List;

import cn.com.codes.framework.app.services.BaseServiceImpl;
import cn.com.codes.framework.security.SecurityPrivilege;
import cn.com.codes.framework.security.Visit;
import cn.com.codes.framework.security.VisitUser;
import cn.com.codes.framework.security.filter.SecurityContext;
import cn.com.codes.framework.security.filter.SecurityContextHolder;
import cn.com.codes.ldapVerification.service.LdapVerificationService;
import cn.com.codes.object.User;

public class LdapVerificationServiceImpl extends BaseServiceImpl implements LdapVerificationService {
	
	private SecurityPrivilege securityPrivilege;

	@Override
	public void saveLdapUserInfo(User user) {
		// TODO Auto-generated method stub
		this.saveOrUpdate(user);
		this.ldapAuthUserLoadPrivilege(user);
	}

	@Override
	public void batchSaveLdapUserInfo(List<User> userList) {
		// TODO Auto-generated method stub
		this.batchSaveOrUpdate(userList);
	}

	@Override
	public void ldapAuthUserLoadPrivilege(User userAuth) {
		Visit visit = SecurityContextHolder.getContext().getVisit();
		if(visit !=null&&!visit.getUserInfo(VisitUser.class).getLoginName().equals(userAuth.getLoginName())){
			visit = new Visit();//这是不是本人重新登录
		} 
		if(visit !=null){
			//同一客户端,登录且有同一session存在 
			SecurityContextHolder.getContext().setAttr("reptLogin", "");
		}  
		if(visit==null){
			visit = new Visit();
		}
		
		VisitUser vu = userAuth.copy2VisitUser();
		SecurityContext sec = SecurityContextHolder.getContext();
		
		sec.setVisit(visit);
		securityPrivilege.setUserPrivilege(vu);
		visit.setUserInfo(vu);
	}

	public SecurityPrivilege getSecurityPrivilege() {
		return securityPrivilege;
	}

	public void setSecurityPrivilege(SecurityPrivilege securityPrivilege) {
		this.securityPrivilege = securityPrivilege;
	}

	

}
