package cn.com.codes.ldapVerification.service;

import java.util.List;

import cn.com.codes.framework.app.services.BaseService;
import cn.com.codes.object.User;

public interface LdapVerificationService extends BaseService{

	void saveLdapUserInfo(User user);

	void batchSaveLdapUserInfo(List<User> userList);

	void ldapAuthUserLoadPrivilege(User userAuth);

}
