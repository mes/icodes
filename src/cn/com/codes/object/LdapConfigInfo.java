package cn.com.codes.object;

import java.util.Date;

import cn.com.codes.framework.transmission.JsonInterface;

public class LdapConfigInfo implements  JsonInterface {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String id;
	private String serverIp;
	private String port;
	private String baseDn;
	private String bindPassword;
	private String createPerson;
	private Date createTime;
	private Date updateTime;
	private String note;
	private String enable;//0、禁用，1、启用

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getServerIp() {
		return serverIp;
	}

	public void setServerIp(String serverIp) {
		this.serverIp = serverIp;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getBaseDn() {
		return baseDn;
	}

	public void setBaseDn(String baseDn) {
		this.baseDn = baseDn;
	}

	public String getBindPassword() {
		return bindPassword;
	}

	public void setBindPassword(String bindPassword) {
		this.bindPassword = bindPassword;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getCreatePerson() {
		return createPerson;
	}

	public void setCreatePerson(String createPerson) {
		this.createPerson = createPerson;
	}

	@Override
	public String toStrUpdateInit() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String toStrList() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String toStrUpdateRest() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void toString(StringBuffer bf) {
		// TODO Auto-generated method stub
		
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getEnable() {
		return enable;
	}

	public void setEnable(String enable) {
		this.enable = enable;
	}

}
