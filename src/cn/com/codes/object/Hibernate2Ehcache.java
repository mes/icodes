package cn.com.codes.object;

import java.io.File;
import java.io.FilenameFilter;

public class Hibernate2Ehcache {

	File[] files = null;

	public Hibernate2Ehcache() {

	}

	public static void main(String[] args) {
		Hibernate2Ehcache gen = new Hibernate2Ehcache();
		gen.gen();
	}

	public void gen() {
		String path = getClass().getResource("").toString();
		path = path.replace("%20", " ");
		path = path + "mapping";
		System.out.println(path);
		path = path.substring(6, path.length());
		System.out.println(path);
		File f = new File(path);
		files = f.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				if (name.endsWith(".xml")) {
					return true;
				}
				return false;
			}
		});
		int i=0;
		for (File file : files) {
			i++;
			StringBuffer sb = new StringBuffer(
					"<cache name=\"cn.com.codes.object.");
			sb.append(file.getName().substring(0, file.getName().indexOf("."))
					+ "\"\n");
			sb.append("    maxElementsInMemory=\"6000\" eternal=\"false\" timeToIdleSeconds=\"4200\"\n");
			sb.append("    timeToLiveSeconds=\"4200\" overflowToDisk=\"false\" />\n");
			System.out.println(sb.toString());
		}
		System.out.println("object count ========"+i);
	}
	
	
}
