/**
 * 
 */
package cn.com.codes.object;

import java.io.Serializable;


public class UserTag implements Serializable{

	/**  
	* 字段:      字段名称
	* @Fields serialVersionUID : TODO 
	*/
	private static final long serialVersionUID = 1L;
	private String userTagId;
	private String userId;
	private String tagName;
	public String getUserTagId() {
		return userTagId;
	}
	public void setUserTagId(String userTagId) {
		this.userTagId = userTagId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getTagName() {
		return tagName;
	}
	public void setTagName(String tagName) {
		this.tagName = tagName;
	}
}
