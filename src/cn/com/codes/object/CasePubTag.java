package cn.com.codes.object;

public class CasePubTag extends TypeDefine {

	public CasePubTag() {
		super();
	}

	public CasePubTag(Long typeId, String typeName) {
		super(typeId, typeName);
	}
	
	public CasePubTag(Long typeId, String typeName,Integer isDefault,String remark,String status){
		super(typeId, typeName,isDefault,remark,status);
	}

}
