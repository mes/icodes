package cn.com.codes.env.util;

import org.apache.http.util.TextUtils;

public class JsonParserUtil {
	
	public enum JSON_TYPE {
		/*JSONObject*/
		JSON_TYPE_OBJECT,
		/*JSONArray*/
		JSON_TYPE_ARRAY,
		/*不是JSON格式*/
		JSON_TYPE_ERROR
	}
	
	public static JSON_TYPE getJSONType(String str){  
        if(TextUtils.isEmpty(str)){  
            return JSON_TYPE.JSON_TYPE_ERROR;  
        }  
  
        final char[] strChar = str.substring(0, 1).toCharArray();  
        final char firstChar = strChar[0];  
  
        if(firstChar == '{'){  
            return JSON_TYPE.JSON_TYPE_OBJECT;  
        }else if(firstChar == '['){  
            return JSON_TYPE.JSON_TYPE_ARRAY;  
        }else{  
            return JSON_TYPE.JSON_TYPE_ERROR;  
        }  
    } 

}
