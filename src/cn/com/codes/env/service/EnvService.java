package cn.com.codes.env.service;

import java.util.List;

import cn.com.codes.env.dto.EnvDto;
import cn.com.codes.framework.app.services.BaseService;
import cn.com.codes.object.Cert;
import cn.com.codes.object.Image;;

public interface EnvService extends BaseService {
	
	public List<Image> getImageList(EnvDto envDto);
	
	public List<Cert> getCertList(EnvDto envDto);
	
	public List<Cert> getCertList();
	
	public Boolean saveImage(EnvDto envDto);
	
	public Boolean saveCert(EnvDto envDto);
	
	public void saveAppInfo(EnvDto envDto);
	
	public void updateAppStatus(EnvDto envDto);
	
	public void saveBuildImageInfo(EnvDto envDto);
	
	public void deleteImage(EnvDto envDto);

}
