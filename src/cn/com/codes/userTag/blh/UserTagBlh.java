package cn.com.codes.userTag.blh;


import java.util.ArrayList;
import java.util.List;

import cn.com.codes.common.util.StringUtils;
import cn.com.codes.framework.app.blh.BusinessBlh;
import cn.com.codes.framework.app.view.View;
import cn.com.codes.framework.common.JsonUtil;
import cn.com.codes.framework.transmission.events.BusiRequestEvent;
import cn.com.codes.object.UserTag;
import cn.com.codes.userTag.dto.UserTagDto;
import cn.com.codes.userTag.service.UserTagService;

public class UserTagBlh extends BusinessBlh {

	private UserTagService userTagService;
	
	public View getUserTagsByUserId(BusiRequestEvent req){
		UserTagDto dto = super.getDto(UserTagDto.class, req);
		UserTag userTag = dto.getUserTag();
		List<UserTag> userTags = new ArrayList<UserTag>();
		if(!StringUtils.isNullOrEmpty(userTag.getUserId())){
			userTags = userTagService.getUserTagsByUserId(userTag.getUserId());
		}
		super.writeResult(JsonUtil.toJson(userTags));
		return super.globalAjax();

	}

	public UserTagService getUserTagService() {
		return userTagService;
	}

	public void setUserTagService(UserTagService userTagService) {
		this.userTagService = userTagService;
	}
	
	
}
