package cn.com.codes.userTag.dto;

import cn.com.codes.framework.transmission.dto.BaseDto;
import cn.com.codes.object.UserTag;

public class UserTagDto extends BaseDto {

	/**  
	* 字段:      字段名称
	* @Fields serialVersionUID : TODO 
	*/
	private static final long serialVersionUID = 1L;
	private UserTag userTag ;
	public UserTagDto(){
		
	}
	public UserTag getUserTag() {
		return userTag;
	}
	public void setUserTag(UserTag userTag) {
		this.userTag = userTag;
	}
	
}
