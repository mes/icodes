package cn.com.codes.userTag.service;

import java.util.List;

import cn.com.codes.framework.app.services.BaseService;
import cn.com.codes.object.UserTag;

public interface UserTagService extends BaseService {

	public String save(UserTag userTag);

	public List<UserTag> getUserTagsByUserId(String userId);
}
