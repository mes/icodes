package cn.com.codes.userTag.web;

import cn.com.codes.framework.app.blh.BaseBizLogicHandler;
import cn.com.codes.framework.exception.BaseException;
import cn.com.codes.framework.transmission.events.BusiRequestEvent;
import cn.com.codes.framework.web.action.BaseAction;
import cn.com.codes.userTag.blh.UserTagBlh;
import cn.com.codes.userTag.dto.UserTagDto;

public class UserTagAction extends BaseAction<UserTagBlh> {

	/**  
	* 字段:      字段名称
	* @Fields serialVersionUID : TODO 
	*/
	private static final long serialVersionUID = 1L;
	private UserTagDto dto = new UserTagDto();
	private UserTagBlh userTagBlh;
	protected void _prepareRequest(BusiRequestEvent reqEvent)
			throws BaseException {
		reqEvent.setDto(dto);

	}
	
	public  BaseBizLogicHandler getBlh(){
		return userTagBlh;
	}

	public UserTagDto getDto() {
		return dto;
	}

	public void setDto(UserTagDto dto) {
		this.dto = dto;
	}

	public UserTagBlh getUserTagBlh() {
		return userTagBlh;
	}

	public void setUserTagBlh(UserTagBlh userTagBlh) {
		this.userTagBlh = userTagBlh;
	}
	
}
