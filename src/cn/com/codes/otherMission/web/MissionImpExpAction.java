package cn.com.codes.otherMission.web;

import java.io.File;
import java.io.InputStream;
import java.util.HashMap;

import cn.com.codes.framework.app.blh.BaseBizLogicHandler;
import cn.com.codes.framework.exception.BaseException;
import cn.com.codes.framework.transmission.events.BusiRequestEvent;
import cn.com.codes.framework.web.action.BaseAction;
import cn.com.codes.otherMission.blh.OtherMissionBlh;
import cn.com.codes.otherMission.dto.OtherMissionDto;

public class MissionImpExpAction extends BaseAction {

	private OtherMissionDto dto ;
	private OtherMissionBlh otherMissionBlh;
	private InputStream inputStream;
	private File importFile;  
	private String importFileContentType; 
	private String importFileFileName; 
	@Override
	protected void _prepareRequest(BusiRequestEvent reqEvent)
			throws BaseException {
		if(dto==null){
			dto = new OtherMissionDto();
		}
		reqEvent.setDto(dto);	

	}
	protected String _processResponse() throws BaseException {
		HashMap<?, ?> displayData = (HashMap<?, ?>) _getDisplayData();
		inputStream = (InputStream) displayData.get("excel");
		if(super.getBlhControlFlow().equals("exportMissions")) {
			return "expSuccess";
		}
		return SUCCESS;
	}
	public OtherMissionDto getDto() {
		return dto;
	}
	public void setDto(OtherMissionDto dto) {
		this.dto = dto;
	}

	
	public  BaseBizLogicHandler getBlh(){
		  
		return otherMissionBlh;
	}
	public InputStream getInputStream() {
		return inputStream;
	}
	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}
	
	public OtherMissionBlh getOtherMissionBlh() {
		return otherMissionBlh;
	}
	public void setOtherMissionBlh(OtherMissionBlh otherMissionBlh) {
		this.otherMissionBlh = otherMissionBlh;
	}
	public File getImportFile() {
		return importFile;
	}
	public void setImportFile(File importFile) {
		this.importFile = importFile;
	}
	public String getImportFileContentType() {
		return importFileContentType;
	}
	public void setImportFileContentType(String importFileContentType) {
		this.importFileContentType = importFileContentType;
	}
	public String getImportFileFileName() {
		return importFileFileName;
	}
	public void setImportFileFileName(String importFileFileName) {
		this.importFileFileName = importFileFileName;
	}

}
