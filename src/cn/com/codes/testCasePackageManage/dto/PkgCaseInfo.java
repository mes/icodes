package cn.com.codes.testCasePackageManage.dto;

import java.util.Date;

public class PkgCaseInfo {

	String pkgCaseId;
	String packageId;
	String taskId;
	Long moduleId;
	Long testCaseId;
	String createrId;
	String prefixCondition;
	String testCaseDes;
	Integer isReleased;
	Date creatdate;
	String attachUrl;
	Long caseTypeId;
	Long priId;
	Integer weight;
	Integer testStatus;
	String moduleNum;
	String auditId;
	String operDataRichText;
	String expResult;
	String taskName;
	String typeName;
	String authorName;
	String auditerNmae;
	String priName;
	public PkgCaseInfo() {

	}

	public PkgCaseInfo(String pkgCaseId, String packageId, String taskId, Long moduleId, Long testCaseId,
			String createrId, String prefixCondition, String testCaseDes, Integer isReleased, Date creatdate,
			String attachUrl, Long caseTypeId, Long priId, Integer weight, Integer testStatus, String moduleNum,
			String auditId, String operDataRichText, String expResult) {
		super();
		this.pkgCaseId = pkgCaseId;
		this.packageId = packageId;
		this.taskId = taskId;
		this.moduleId = moduleId;
		this.testCaseId = testCaseId;
		this.createrId = createrId;
		this.prefixCondition = prefixCondition;
		this.testCaseDes = testCaseDes;
		this.isReleased = isReleased;
		this.creatdate = creatdate;
		this.attachUrl = attachUrl;
		this.caseTypeId = caseTypeId;
		this.priId = priId;
		this.weight = weight;
		this.testStatus = testStatus;
		this.moduleNum = moduleNum;
		this.auditId = auditId;
		this.operDataRichText = operDataRichText;
		this.expResult = expResult;
	}

	public String getPkgCaseId() {
		return pkgCaseId;
	}

	public void setPkgCaseId(String pkgCaseId) {
		this.pkgCaseId = pkgCaseId;
	}

	public String getPackageId() {
		return packageId;
	}

	public void setPackageId(String packageId) {
		this.packageId = packageId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public Long getModuleId() {
		return moduleId;
	}

	public void setModuleId(Long moduleId) {
		this.moduleId = moduleId;
	}

	public Long getTestCaseId() {
		return testCaseId;
	}

	public void setTestCaseId(Long testCaseId) {
		this.testCaseId = testCaseId;
	}

	public String getCreaterId() {
		return createrId;
	}

	public void setCreaterId(String createrId) {
		this.createrId = createrId;
	}

	public String getPrefixCondition() {
		return prefixCondition;
	}

	public void setPrefixCondition(String prefixCondition) {
		this.prefixCondition = prefixCondition;
	}

	public String getTestCaseDes() {
		return testCaseDes;
	}

	public void setTestCaseDes(String testCaseDes) {
		this.testCaseDes = testCaseDes;
	}

	public Integer getIsReleased() {
		return isReleased;
	}

	public void setIsReleased(Integer isReleased) {
		this.isReleased = isReleased;
	}

	public Date getCreatdate() {
		return creatdate;
	}

	public void setCreatdate(Date creatdate) {
		this.creatdate = creatdate;
	}

	public String getAttachUrl() {
		return attachUrl;
	}

	public void setAttachUrl(String attachUrl) {
		this.attachUrl = attachUrl;
	}

	public Long getCaseTypeId() {
		return caseTypeId;
	}

	public void setCaseTypeId(Long caseTypeId) {
		this.caseTypeId = caseTypeId;
	}

	public Long getPriId() {
		return priId;
	}

	public void setPriId(Long priId) {
		this.priId = priId;
	}

	public Integer getWeight() {
		return weight;
	}

	public void setWeight(Integer weight) {
		this.weight = weight;
	}

	public Integer getTestStatus() {
		return testStatus;
	}

	public void setTestStatus(Integer testStatus) {
		this.testStatus = testStatus;
	}

	public String getModuleNum() {
		return moduleNum;
	}

	public void setModuleNum(String moduleNum) {
		this.moduleNum = moduleNum;
	}

	public String getAuditId() {
		return auditId;
	}

	public void setAuditId(String auditId) {
		this.auditId = auditId;
	}

	public String getOperDataRichText() {
		return operDataRichText;
	}

	public void setOperDataRichText(String operDataRichText) {
		this.operDataRichText = operDataRichText;
	}

	public String getExpResult() {
		return expResult;
	}

	public void setExpResult(String expResult) {
		this.expResult = expResult;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	public String getAuditerNmae() {
		return auditerNmae;
	}

	public void setAuditerNmae(String auditerNmae) {
		this.auditerNmae = auditerNmae;
	}

	public String getPriName() {
		return priName;
	}

	public void setPriName(String priName) {
		this.priName = priName;
	}
}
