package cn.com.codes.testCasePackageManage.dto;

import org.codehaus.jackson.annotate.JsonIgnore;

public class PkgTestCaseReal {
	

	private Long testCaseId;
	@JsonIgnore
	private String packageId;

	public PkgTestCaseReal() {
		
	}
	
	public PkgTestCaseReal(Long testCaseId,String packageId) {
		this.testCaseId = testCaseId;
		this.packageId= packageId;
	}
	public Long getTestCaseId() {
		return testCaseId;
	}

	public void setTestCaseId(Long testCaseId) {
		this.testCaseId = testCaseId;
	}

	public String getPackageId() {
		return packageId;
	}

	public void setPackageId(String packageId) {
		this.packageId = packageId;
	}


}
