/**
 * 
 */
package cn.com.codes.testCasePackageManage.service;

import java.util.List;

import cn.com.codes.framework.app.services.BaseService;
import cn.com.codes.object.TestCaseInfo;
import cn.com.codes.overview.dto.DataVo;
import cn.com.codes.testCasePackageManage.dto.PkgCaseInfo;
import cn.com.codes.testCasePackageManage.dto.PkgTestCaseReal;
import cn.com.codes.testCasePackageManage.dto.TestCasePackageDto;


public interface TestCasePackageService extends BaseService{
	
	
	public void saveTestCasePackage(TestCasePackageDto testCasePackageDto);
	
	public void updateTestCasePackage(TestCasePackageDto testCasePackageDto);
	
	public void deleteTestCasePkgById(String packageId);
	
	public String[] getUserIdsByPackageId(TestCasePackageDto testCasePackageDto);
	
	public String[] getTestCaseIdsByPackageId(TestCasePackageDto testCasePackageDto);
	
	public List<PkgTestCaseReal> getSelTestCasesByPkgId(TestCasePackageDto testCasePackageDto);
	
	public  List<PkgCaseInfo> getPaginationForSelTestCasesByPkgId(TestCasePackageDto testCasePackageDto);
	
	public void saveTestCase_CasePkg(TestCasePackageDto testCasePackageDto);
	
	public boolean reNameChkInTask(String objName,final String nameVal,final String namePropName,String idPropName,final String idPropVal,final String taskId);	

	public DataVo getBugStaticsByPkgId(TestCasePackageDto testCasePackageDto);
	
	public void delTestCase_CasePkgByPkgCaseId(String pkgCaseId,String selCaseId,String packageId);
	
	public void updateTestCasePkgExeCount(String testCasepkgId);
	
	public void reWriteEexRatio(String testCasepkgId);
	
	public List<TestCaseInfo> findCaseIdByModuleIds(List<String> moduleNumList,String taskId,String tagName);
	
}
