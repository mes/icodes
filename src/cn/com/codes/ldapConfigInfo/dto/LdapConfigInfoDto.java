package cn.com.codes.ldapConfigInfo.dto;

import cn.com.codes.framework.transmission.dto.BaseDto;
import cn.com.codes.object.LdapConfigInfo;

public class LdapConfigInfoDto extends BaseDto{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private LdapConfigInfo ldapConfigInfo;
	
	
	
	

	public LdapConfigInfo getLdapConfigInfo() {
		return ldapConfigInfo;
	}

	public void setLdapConfigInfo(LdapConfigInfo ldapConfigInfo) {
		this.ldapConfigInfo = ldapConfigInfo;
	}

}
