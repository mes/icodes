package cn.com.codes.ldapConfigInfo.service.impl;

import java.util.Date;
import java.util.List;

import cn.com.codes.framework.app.services.BaseServiceImpl;
import cn.com.codes.ldapConfigInfo.dto.LdapConfigInfoDto;
import cn.com.codes.ldapConfigInfo.service.LdapConfigInfoService;
import cn.com.codes.ldapVerification.dto.LdapVerificationDto;
import cn.com.codes.ldapVerification.util.LdapInitUtil;
import cn.com.codes.object.LdapConfigInfo;

public class LdapConfigInfoServiceImpl extends BaseServiceImpl implements LdapConfigInfoService {
	
	private LdapInitUtil ldapInitUtil;

	@Override
	public void saveLdapConfigInfo(LdapConfigInfoDto dto) {
		LdapConfigInfo ldapConfigInfo = dto.getLdapConfigInfo();
		ldapConfigInfo.setCreateTime(new Date());
		if(null != ldapConfigInfo){
			this.add(ldapConfigInfo);
		}
	}

	@Override
	public void updateLdapConfigInfo(LdapConfigInfoDto dto) {
		LdapConfigInfo ldapConfigInfo = dto.getLdapConfigInfo();
		List<LdapConfigInfo> ldapInfos = this.findByProperties(LdapConfigInfo.class, new String[]{"id"}, new Object[]{ldapConfigInfo.getId()});
		if(null != ldapInfos && ldapInfos.size() > 0){
			LdapConfigInfo configInfo = ldapInfos.get(0);
			configInfo.setServerIp(ldapConfigInfo.getServerIp());
			configInfo.setPort(ldapConfigInfo.getPort());
			configInfo.setBaseDn(ldapConfigInfo.getBaseDn());
			configInfo.setBindPassword(ldapConfigInfo.getBindPassword());
			configInfo.setNote(ldapConfigInfo.getNote());
			configInfo.setUpdateTime(new Date());
			this.saveOrUpdate(configInfo);
			
			
		}
	}

	public LdapInitUtil getLdapInitUtil() {
		return ldapInitUtil;
	}

	public void setLdapInitUtil(LdapInitUtil ldapInitUtil) {
		this.ldapInitUtil = ldapInitUtil;
	}

}
