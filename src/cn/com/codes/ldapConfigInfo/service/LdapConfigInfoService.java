package cn.com.codes.ldapConfigInfo.service;

import cn.com.codes.framework.app.services.BaseService;
import cn.com.codes.ldapConfigInfo.dto.LdapConfigInfoDto;

public interface LdapConfigInfoService extends BaseService{

	void saveLdapConfigInfo(LdapConfigInfoDto dto);

	void updateLdapConfigInfo(LdapConfigInfoDto dto);

}
