package cn.com.codes.ldapConfigInfo.web;

import cn.com.codes.framework.app.blh.BaseBizLogicHandler;
import cn.com.codes.framework.exception.BaseException;
import cn.com.codes.framework.transmission.events.BusiRequestEvent;
import cn.com.codes.framework.web.action.BaseAction;
import cn.com.codes.ldapConfigInfo.blh.LdapConfigInfoBlh;
import cn.com.codes.ldapConfigInfo.dto.LdapConfigInfoDto;

public class LdapConfigInfoAction extends BaseAction<LdapConfigInfoBlh>{

	private static final long serialVersionUID = 1L;
	private LdapConfigInfoBlh ldapConfigInfoBlh;
	private LdapConfigInfoDto ldapConfigInfoDto = new LdapConfigInfoDto();

	public LdapConfigInfoBlh getLdapConfigInfoBlh() {
		return ldapConfigInfoBlh;
	}

	public void setLdapConfigInfoBlh(LdapConfigInfoBlh ldapConfigInfoBlh) {
		this.ldapConfigInfoBlh = ldapConfigInfoBlh;
	}

	public LdapConfigInfoDto getLdapConfigInfoDto() {
		return ldapConfigInfoDto;
	}

	public void setLdapConfigInfoDto(LdapConfigInfoDto ldapConfigInfoDto) {
		this.ldapConfigInfoDto = ldapConfigInfoDto;
	}

	@Override
	protected void _prepareRequest(BusiRequestEvent reqEvent)
			throws BaseException {
		reqEvent.setDto(ldapConfigInfoDto);
	}
	
	public  BaseBizLogicHandler getBlh(){
		return ldapConfigInfoBlh;
	}

}
