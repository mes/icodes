package cn.com.codes.ldapConfigInfo.blh;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import cn.com.codes.common.dto.PageModel;
import cn.com.codes.common.util.StringUtils;
import cn.com.codes.framework.app.blh.BusinessBlh;
import cn.com.codes.framework.app.view.View;
import cn.com.codes.framework.common.JsonUtil;
import cn.com.codes.framework.security.VisitUser;
import cn.com.codes.framework.security.filter.SecurityContextHolder;
import cn.com.codes.framework.transmission.events.BusiRequestEvent;
import cn.com.codes.ldapConfigInfo.dto.LdapConfigInfoDto;
import cn.com.codes.ldapConfigInfo.service.LdapConfigInfoService;
import cn.com.codes.ldapVerification.dto.LdapVerificationDto;
import cn.com.codes.ldapVerification.util.LdapInitUtil;
import cn.com.codes.object.LdapConfigInfo;

public class LdapConfigInfoBlh extends BusinessBlh {
	
	@SuppressWarnings("unused")
	private static Logger logger = Logger.getLogger(LdapConfigInfoBlh.class);
	private LdapConfigInfoBlh ldapConfigInfoBlh;
	private LdapConfigInfoService ldapConfigInfoService;
	private LdapInitUtil ldapInitUtil;
	
	public View ldapServerConfigList(BusiRequestEvent req){
		return super.getView();
	}
	
	@SuppressWarnings("unchecked")
	public View ldapServerConfigInfo(BusiRequestEvent req){
		LdapConfigInfoDto dto = super.getDto(LdapConfigInfoDto.class, req);
		Integer isAdmin = SecurityContextHolder.getContext().getVisit().getUserInfo(VisitUser.class).getIsAdmin();
		String loginName = SecurityContextHolder.getContext().getVisit().getUserInfo(VisitUser.class).getLoginName();
		PageModel pageModel = new PageModel(); 
		
		if("admin".equals(loginName) && isAdmin == 1){
			this.buildLdapServerConfigInfoSql(dto);
			pageModel.setQueryHql(dto.getHql());
			pageModel.setPageNo(dto.getPageNo());
			pageModel.setPageSize(dto.getPageSize());
			pageModel.setHqlParamMap(dto.getHqlParamMaps());
			
			ldapConfigInfoService.getJdbcTemplateWrapper().fillPageModelData(pageModel, LdapConfigInfo.class, "id");
			List<LdapConfigInfo> ldapConfigInfos = (List<LdapConfigInfo>) pageModel.getRows();
			if(null != ldapConfigInfos && ldapConfigInfos.size() > 0){
				pageModel.setRows(ldapConfigInfos);
				pageModel.setTotal(ldapConfigInfos.size());
			}else{
				pageModel.setRows(new ArrayList<LdapConfigInfo>());
				pageModel.setTotal(0);
			}
		}
		
		super.writeResult(JsonUtil.toJson(pageModel));
		return super.globalAjax();
	}
	
	
	private void buildLdapServerConfigInfoSql(LdapConfigInfoDto dto) {
		LdapConfigInfo ldapConfigInfo = dto.getLdapConfigInfo();
		StringBuffer sql = new StringBuffer();
		Map<String,Object> hashMap = new HashMap<String,Object>();
		sql.append(" SELECT tp.* FROM t_ldap_config_info tp WHERE 1=1 ");
		
		if(null != ldapConfigInfo){
			if(!StringUtils.isNullOrEmpty(ldapConfigInfo.getServerIp())){
				sql.append("and tp.server_ip = :serverIp");
				hashMap.put("serverIp", ldapConfigInfo.getServerIp());
			}
			
			if(!StringUtils.isNullOrEmpty(ldapConfigInfo.getBindPassword())){
				sql.append("and tp.bind_password = :bindPassword");
				hashMap.put("bindPassword", ldapConfigInfo.getBindPassword());
			}
			
			if(!StringUtils.isNullOrEmpty(ldapConfigInfo.getPort())){
				sql.append("and tp.port = :port");
				hashMap.put("port", ldapConfigInfo.getPort());
			}
			
			if(!StringUtils.isNullOrEmpty(ldapConfigInfo.getBaseDn())){
				sql.append("and tp.base_dn like :baseDn");
				hashMap.put("baseDn", "%"+ldapConfigInfo.getBaseDn()+"%");
			}
			
			if(!StringUtils.isNullOrEmpty(ldapConfigInfo.getNote())){
				sql.append("and tp.note like :note");
				hashMap.put("note", "%"+ldapConfigInfo.getNote()+"%");
			}
		}
		
		dto.setHql(sql.toString());
		dto.setHqlParamMaps(hashMap);
	}
	
	public View saveOrUpdateLdapConfigInfo(BusiRequestEvent req){
		LdapConfigInfoDto dto = super.getDto(LdapConfigInfoDto.class, req);
		LdapConfigInfo ldapConfigInfo = dto.getLdapConfigInfo();
		LdapVerificationDto ldapVerificationDto = new LdapVerificationDto();
		ldapVerificationDto.setLdapConfigInfo(ldapConfigInfo);
		if(StringUtils.isNullOrEmpty(ldapConfigInfo.getId())){
			//保存ldap服务配置信息
			ldapConfigInfoService.saveLdapConfigInfo(dto);
		}else{
			//更新ldap服务配置信息
			ldapConfigInfoService.updateLdapConfigInfo(dto);
		}
		
		ldapInitUtil.ldapConfigInfoEditStartEnable(ldapVerificationDto);
		
		super.writeResult("success");
		return super.globalAjax();
	}
	
	public View deleteLdapConfigInfo(BusiRequestEvent req){
		LdapConfigInfoDto dto = super.getDto(LdapConfigInfoDto.class, req);
		LdapConfigInfo ldapConfigInfo = dto.getLdapConfigInfo();
		if(!"".equals(ldapConfigInfo.getId())){
			//保存ldap服务配置信息
			ldapConfigInfoService.delete(LdapConfigInfo.class, ldapConfigInfo.getId());
		}
		
		super.writeResult("success");
		return super.globalAjax();
	}
	
	public View searchLdapConfigInfo(BusiRequestEvent req){
		LdapConfigInfoDto dto = super.getDto(LdapConfigInfoDto.class, req);
		LdapConfigInfo ldapConfigInfo = dto.getLdapConfigInfo();
		List<LdapConfigInfo> ldapConfigInfos = null;
		if(!"".equals(ldapConfigInfo.getId())){
			ldapConfigInfos = ldapConfigInfoService.findByProperties(LdapConfigInfo.class, new String[]{"id"}, new Object[]{ldapConfigInfo.getId()});
		}
		
		super.writeResult(JsonUtil.toJson(ldapConfigInfos.get(0)));
		return super.globalAjax();
	}
	
	@SuppressWarnings("static-access")
	public View editLdapStatus(BusiRequestEvent req){
		LdapConfigInfoDto dto = super.getDto(LdapConfigInfoDto.class, req);
		LdapConfigInfo ldapConfigInfo = dto.getLdapConfigInfo();
		List<LdapConfigInfo> ldapConfigInfos = null;
		if(!"".equals(ldapConfigInfo.getId())){
			ldapConfigInfos = ldapConfigInfoService.findByProperties(LdapConfigInfo.class, new String[]{"id"}, new Object[]{ldapConfigInfo.getId()});
			if(null != ldapConfigInfos && ldapConfigInfos.size() > 0){
				LdapConfigInfo configInfo = ldapConfigInfos.get(0);
				configInfo.setEnable(ldapConfigInfo.getEnable());
				configInfo.setUpdateTime(new Date());
				
				ldapConfigInfoService.update(configInfo);
				
				if("0".equals(ldapConfigInfo.getEnable())){
					ldapInitUtil.setLdapDataBase(null);
				}else{
					LdapVerificationDto ldapVerificationDto = new LdapVerificationDto();
					ldapVerificationDto.setLdapConfigInfo(configInfo);
					ldapInitUtil.ldapConfigInfoEditStartEnable(ldapVerificationDto);
				}
				
			}
		}
		
		super.writeResult("success");
		return super.globalAjax();
	}
	
	@SuppressWarnings("unchecked")
	public View findLdapIsEnableRecord(BusiRequestEvent req){
		LdapConfigInfoDto dto = super.getDto(LdapConfigInfoDto.class, req);
		
		List<LdapConfigInfo> ldapConfigInfos = ldapConfigInfoService.findBySql("select * from t_ldap_config_info t WHERE t.enable = ? ", LdapConfigInfo.class, "1");
		if(null != ldapConfigInfos && ldapConfigInfos.size() > 0){
			super.writeResult(JsonUtil.toJson(ldapConfigInfos.get(0)));
		}else{
			super.writeResult(JsonUtil.toJson(new ArrayList<LdapConfigInfo>()));
		}
		return super.globalAjax();
	}
	


	public LdapConfigInfoBlh getLdapConfigInfoBlh() {
		return ldapConfigInfoBlh;
	}
	public void setLdapConfigInfoBlh(LdapConfigInfoBlh ldapConfigInfoBlh) {
		this.ldapConfigInfoBlh = ldapConfigInfoBlh;
	}
	public LdapConfigInfoService getLdapConfigInfoService() {
		return ldapConfigInfoService;
	}
	public void setLdapConfigInfoService(LdapConfigInfoService ldapConfigInfoService) {
		this.ldapConfigInfoService = ldapConfigInfoService;
	}

	public LdapInitUtil getLdapInitUtil() {
		return ldapInitUtil;
	}

	public void setLdapInitUtil(LdapInitUtil ldapInitUtil) {
		this.ldapInitUtil = ldapInitUtil;
	}
	
}
