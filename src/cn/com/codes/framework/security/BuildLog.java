package cn.com.codes.framework.security;

public interface BuildLog {

	public SysLog buildLog(SysLogConfigure configure, Object[] objs, String accountName);
}
