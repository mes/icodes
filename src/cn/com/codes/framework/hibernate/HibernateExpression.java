
package cn.com.codes.framework.hibernate;

import org.hibernate.criterion.Criterion;

/**
 * @author liuyg
 *
 */
public interface HibernateExpression {
	public Criterion createCriteria();
}
