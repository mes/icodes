package cn.com.codes.testCaseLibManager.web;

import cn.com.codes.framework.app.blh.BaseBizLogicHandler;
import cn.com.codes.framework.exception.BaseException;
import cn.com.codes.framework.transmission.events.BusiRequestEvent;
import cn.com.codes.framework.web.action.BaseAction;
import cn.com.codes.testCaseLibManager.blh.TestCaseLibManagerBlh;
import cn.com.codes.testCaseLibManager.dto.TestCaseLibManagerDto;

public class TestCaseLibAction extends BaseAction<TestCaseLibManagerBlh> {


	private static final long serialVersionUID = 1L;
	private TestCaseLibManagerDto dto = new TestCaseLibManagerDto();
	private TestCaseLibManagerBlh testCaseLibManagerBlh  ;
	@Override
	protected void _prepareRequest(BusiRequestEvent reqEvent) throws BaseException {
		
		reqEvent.setDto(dto);
	}
	public  BaseBizLogicHandler getBlh(){
		return testCaseLibManagerBlh;
	}

	public TestCaseLibManagerDto getDto() {
		return dto;
	}
	public void setDto(TestCaseLibManagerDto dto) {
		this.dto = dto;
	}
	
	public TestCaseLibManagerBlh getTestCaseLibManagerBlh() {
		return testCaseLibManagerBlh;
	}
	public void setTestCaseLibManagerBlh(TestCaseLibManagerBlh testCaseLibManagerBlh) {
		this.testCaseLibManagerBlh = testCaseLibManagerBlh;
	}
}
