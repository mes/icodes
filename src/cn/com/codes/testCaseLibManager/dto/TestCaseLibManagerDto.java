package cn.com.codes.testCaseLibManager.dto;

import cn.com.codes.framework.transmission.dto.BaseDto;
import cn.com.codes.object.CaseLibTestCaseInfo;

public class TestCaseLibManagerDto extends BaseDto {


	private static final long serialVersionUID = 1L;

	private CaseLibTestCaseInfo  testCase;
	
	private Long currNodeId ;
	private Integer currLevel;
	private Integer initLevel;
	private String command ;
	private Long parentNodeId ;
	private Long targetId ;
	private String categoryName ;
	private Integer categoryState;
	private String categoryNum;
	private Integer currSeq;
	private Boolean isMoveOpera =false;
	private String companyId;
	private String[] categoryData = new String[10];
	//传来需要审核的多个用例的主键
	private String testCaseIds;
	//审核状态
	private String examineState;
	//审核人
	private String examineUserId;

	public CaseLibTestCaseInfo getTestCase() {
		return testCase;
	}

	public void setTestCase(CaseLibTestCaseInfo testCase) {
		this.testCase = testCase;
	}

	public Long getCurrNodeId() {
		return currNodeId;
	}

	public void setCurrNodeId(Long currNodeId) {
		this.currNodeId = currNodeId;
	}

	public Integer getCurrLevel() {
		return currLevel;
	}

	public void setCurrLevel(Integer currLevel) {
		this.currLevel = currLevel;
	}

	public Integer getInitLevel() {
		return initLevel;
	}

	public void setInitLevel(Integer initLevel) {
		this.initLevel = initLevel;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public Long getParentNodeId() {
		return parentNodeId;
	}

	public void setParentNodeId(Long parentNodeId) {
		this.parentNodeId = parentNodeId;
	}

	public Long getTargetId() {
		return targetId;
	}

	public void setTargetId(Long targetId) {
		this.targetId = targetId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public Integer getCategoryState() {
		return categoryState;
	}

	public void setCategoryState(Integer categoryState) {
		this.categoryState = categoryState;
	}

	public String getCategoryNum() {
		return categoryNum;
	}

	public void setCategoryNum(String categoryNum) {
		this.categoryNum = categoryNum;
	}

	public Integer getCurrSeq() {
		return currSeq;
	}

	public void setCurrSeq(Integer currSeq) {
		this.currSeq = currSeq;
	}

	public Boolean getIsMoveOpera() {
		return isMoveOpera;
	}

	public void setIsMoveOpera(Boolean isMoveOpera) {
		this.isMoveOpera = isMoveOpera;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String[] getCategoryData() {
		return categoryData;
	}

	public void setCategoryData(String[] categoryData) {
		this.categoryData = categoryData;
	}

	public String getTestCaseIds() {
		return testCaseIds;
	}

	public void setTestCaseIds(String testCaseIds) {
		this.testCaseIds = testCaseIds;
	}

	public String getExamineState() {
		return examineState;
	}

	public void setExamineState(String examineState) {
		this.examineState = examineState;
	}

	public String getExamineUserId() {
		return examineUserId;
	}

	public void setExamineUserId(String examineUserId) {
		this.examineUserId = examineUserId;
	}
}
