<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <style>
 .selectContainer{
  width: 70%;
  /* height:200px; */
  float:left;
  position: relative;
  padding:0;
  margin:0;
  box-sizing: border-box;
 }
 .selectedContent{
  width:85%;
  height: 32px;
  float:left;   
 }
 .dropDown-toggle{
  width:14%;
  height:31px;
  line-height: 31px;
  text-align: center;
  border: 1px solid silver;
  border-left:none;
  float:left;
  padding:0;
  margin:0;
  box-sizing: border-box;
  cursor: pointer;
 }
 .dropDown-menus{
  margin:0;
  padding:0 15px 10px;
  width:100%;
  border:1px solid silver;
  border-top: none;
  box-sizing: border-box;
  list-style: none;
  position: absolute;
  top:31px;
  right:0;
  background-color: white;
  z-index:2000;
  height: 100px;
  overflow-y:auto;
 }
 .items{
  margin-top:8px;
  padding: 2px;
  cursor: pointer;
 }
 .items:hover{
  background: #ddd;
 }
 .isSelectedText{
  display: inline-block;
  width:90%;
 }
 .dsn{
  display: none;
 }
</style>
	<ul class="nav nav-lattice">
		<li class="active"><a href="#caseDetailInfo" data-toggle="tab">基本信息</a></li>
		<li onclick="caseHistory()"><a href="#caseHistory" data-toggle="tab">执行历史</a></li>
		<li id="casefileLi" onclick="lookCaseFile()"><a href="#caseLookFiles" data-toggle="tab">附件信息</a><img id="messageImg" class="messagecss" src="<%=request.getContextPath()%>/itest/images/fj.png" style="display: none;"><span id="fileCount" class="fileCount"></span></li>
	</ul>
	<div class="tab-content" style="margin-top:20px;height: 450px;"> 
		<div id="caseDetailInfo" class="tab-pane fade in active">
			<form id="lookForm" method="post">
				<table class="form-table">
					<tr class="hidden"><td>
						<input type="hidden" name="mdPath" value="" />
						<input itype="hidden" name="dto.testCaseInfo.taskId"/>
						<input type="hidden" name="dto.testCaseInfo.moduleId"/>
						<input type="hidden" name="dto.testCaseInfo.createrId"/>
						<input type="hidden" name="dto.testCaseInfo.testCaseId"/>
						<input type="hidden" name="dto.testCaseInfo.isReleased"/>
						<input type="hidden" name="dto.testCaseInfo.creatdate"/>
						<input type="hidden" name="dto.testCaseInfo.attachUrl"/>
						<input type="hidden" name="dto.testCaseInfo.auditId"/>
						<input type="hidden" name="dto.testCaseInfo.testStatus"/>
						<input type="hidden" name="dto.testCaseInfo.testData"/>
						<input type="hidden" name="dto.testCaseInfo.moduleNum"/>
					</td></tr>
					<tr >
						<td class="left_td"><sup>*</sup>类别:</td>
						<td style="width:130px">
							<input class="exui-combobox caseTypeId" name="dto.testCaseInfo.caseTypeId" data-options="
			    			required:true,
			    			readonly:true,
			    			validateOnCreate:false,
			    			editable:false,
			    			valueField:'typeId',
    						textField:'typeName',
			    			prompt:'-请选择-'" style="width:120px"/>
						</td>
						<td class="left_td">基线:</td>
						<td style="width:130px">
							<input class="exui-combobox baseline" name="dto.testCaseInfo.baseline" data-options="
			    			required:false,
			    			readonly:true,
			    			editable:false,
			    			validateOnCreate:false,
			    			prompt:'-请选择-'" style="width:120px"/>
						</td>
						<td class="left_td" style="padding-left: 0.5em;"><sup>*</sup>优先级:</td>
						<td style="width:130px">
							<input class="exui-combobox priId" name="dto.testCaseInfo.priId" data-options="
			    			required:true,
			    			readonly:true,
			    			validateOnCreate:false,
			    			editable:false,
			    			valueField:'typeId',
	    					textField:'typeName',
			    			prompt:'-请选择-'" style="width:120px"/>
						</td>
						<td class="left_td"> &nbsp;&nbsp;<sup>*</sup>执行成本: </td>
						<td style="width:130px">
							<input name="dto.testCaseInfo.weight"  class="exui-numberbox" data-options="readonly:true,min:1,max:10,required:true" style="width: 50px;">
							<font color="orange" style="font-size: 11px;">一成本5分钟</font>
						</td>
					</tr>
					<tr >
						<td class="left_td"><sup>*</sup>用例描述:</td>
						<td class="dataM_left" colspan="8">
							<input name="dto.testCaseInfo.testCaseDes" class="exui-textbox" data-options="readonly:true,required:true" style="width: 729px;">
						</td>
					</tr>
					<tr style="display:none">
						<td  class="left_td">前置条件:</td>
						<td colspan="8">
							<input name="dto.testCaseInfo.prefixCondition" class="exui-textbox" style="width: 729px;" data-options="readonly:true,multiline:false,prompt:'前置条件'" />
	   			      </td>
					</tr>
					<tr >
						<td class="left_td"><sup>*</sup>过程及数据:</td>
						<td colspan="8" style="background-color: #f7f7f7;">
							<!-- <textarea name="dto.testCaseInfo.operDataRichText" id="operDataRichText"
								cols="50" rows="15" style="width: 100%;">
		   			      </textarea> -->
		   			      <input name="dto.testCaseInfo.operDataRichText" class="exui-textbox" style="width: 729px;height: 160px;" data-options="readonly:true,multiline:true,required:true,prompt:'请填写过程及数据'" />
	   			      	</td>
					</tr>
					<tr >
						<td  class="left_td"><sup>*</sup>期望结果:</td>
						<td class="dataM_left" colspan="8">
							<input name="dto.testCaseInfo.expResult" class="exui-textbox" style="width: 729px;height: 160px;margin-top: 10px;" data-options="readonly:true,multiline:true,required:true,prompt:'请填写期望结果'" />
	   			      </td>
					</tr>
					<tr >
						<td class="left_td">备注:</td>
						<td colspan="8">
							<input name="dto.testCaseInfo.remark" class="exui-textbox" style="width:729px;" data-options="readonly:true,multiline:false,prompt:'备注'" />
	   			      </td>
					</tr>
				<tr>
					<td class="left_td">标签:</td>
					<td  colspan="8">
						<!-- <input id="tagsValueField" name="dto.testCaseInfo.tags" style="display:none"/>
		  				<ul id="tagsField" style="width:699px"></ul> -->
		  				<div class="multipleSelect selectContainer">
						  <input id="userTagsInput" type="text" class="selectedContent" placeholder="可选择也可输入，标签之间用空格隔开" name="dto.testCaseInfo.tags"/>
					
						   <!-- <li class="items">
						    <span class="isSelectedText">苹果</span>
						    <span class="isSelected"><input type="checkbox"></span>
						   </li>
						   <li class="items">     
						    <span class="isSelectedText">梨</span>
						    <span class="isSelected"><input type="checkbox"></span>
						   </li>
						   <li class="items">
						    <span class="isSelectedText">橘子</span>
						    <span class="isSelected"><input type="checkbox"></span>
						   </li>
						   <li style="text-align: right">
						    <button type="button" class="confirmSelect l-btn btn btn-primary btn-default btn-xs exui-linkbutton">确定</button>
						   </li> -->
						  </ul>
						 </div>
	  			    </td>
				</tr>
				</table>
		   </form>
		</div>
		
		<div id="caseLookFiles" class="tab-pane fade" style="padding-bottom: 1em;">
			<h4 id="lookMsg" align="center" style="display: none;">暂无附件信息</h4>
			<form id="lookFilesForm" enctype="multipart/form-data">
		        <div class="form-group">
		            <div class="file-loading">
		                <input id="caseLookFileId" type="file" multiple class="file">
		            </div>
		        </div>
		    </form>
		</div>
		
		<div id="caseHistory" class="tab-pane fade">
		</div>
		<table id="caseHistoryList" class="exui-datagrid" data-options=""></table>
	</div>
	
	<script src="<%=request.getContextPath()%>/itest/js/testCaseManager/lookCase.js" type="text/javascript" charset="utf-8"></script>