<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

    <style>
		.btn-default.active, .btn-default:active, .open>.dropdown-toggle.btn-default {
		    color: #1e90fc/*#333*/;
		    background-color: #ffffff/*#e6e6e6*/;
		    border-color: #1e7cfb;
		}
    
       /* .container{
  margin: 20px auto;
  padding:0 15px;
  width: 50%; 
  height:300px;
  box-sizing: border-box;  
 }
 .text-container{
  display: inline-block;
  float:left;
  width: 15%;
  height: 32px;
  line-height: 32px;
  box-sizing: border-box;
 } */
 .selectContainer{
  width: 70%;
  /* height:200px; */
  float:left;
  position: relative;
  padding:0;
  margin:0;
  box-sizing: border-box;
 }
 .selectedContent{
  width:85%;
  height: 32px;
  float:left;   
 }
 .dropDown-toggle{
  width:14%;
  height:31px;
  line-height: 31px;
  text-align: center;
  border: 1px solid silver;
  border-left:none;
  float:left;
  padding:0;
  margin:0;
  box-sizing: border-box;
  cursor: pointer;
 }
 .dropDown-menus{
  margin:0;
  padding:0 15px 10px;
  width:100%;
  border:1px solid silver;
  border-top: none;
  box-sizing: border-box;
  list-style: none;
  position: absolute;
  top:31px;
  right:0;
  background-color: white;
  z-index:2000;
  height: 100px;
  overflow-y:auto;
 }
 .items{
  margin-top:8px;
  padding: 2px;
  cursor: pointer;
 }
 .items:hover{
  background: #ddd;
 }
 .isSelectedText{
  display: inline-block;
  width:90%;
 }
 .dsn{
  display: none;
 }
 .form-table tr {
	    height: 2.5em;
	}
    </style>
    <ul class="nav nav-lattice">
		<li class="active"><a href="#caseInfo" data-toggle="tab">基本信息</a></li>
		<li onclick="caseHistory()"><a href="#caseHistory" data-toggle="tab">执行历史</a></li>
		<li id="caseEditFileLi" onclick="editCaseFile()">
			<a href="#caseEditFiles" data-toggle="tab">附件信息</a>
			<img id="editImg" class="messagecss" src="<%=request.getContextPath()%>/itest/images/fj.png" style="display: none;">
			<span id="editCount" class="fileCount"></span>
		</li>
	</ul>
	<div class="tab-content" style="margin-top:20px;height: 450px;"> 
		<div id="caseInfo" class="tab-pane fade in active">
			<form id="editCaseForm" method="post">
			<table class="form-table">
				<tr class="hidden"><td>
					<input type="hidden" id="mdPath" name="mdPath" value="" />
					<input id="task_Id" type="hidden" name="dto.testCaseInfo.taskId"/>
					<input id="moduleId" type="hidden" name="dto.testCaseInfo.moduleId"/>
					<input id="createrId" type="hidden" name="dto.testCaseInfo.createrId"/>
					<input id="testCaseId" type="hidden" name="dto.testCaseInfo.testCaseId"/>
					<input id="isReleased" type="hidden" name="dto.testCaseInfo.isReleased"/>
					<input id="creatdate" type="hidden" name="dto.testCaseInfo.creatdate"/>
					<input id="attachUrl" type="hidden" name="dto.testCaseInfo.attachUrl"/>
					<input id="auditId" type="hidden" name="dto.testCaseInfo.auditId"/>
					<input id="testStatus" type="hidden" name="dto.testCaseInfo.testStatus"/>
					<input id="testData" type="hidden" name="dto.testCaseInfo.testData"/>
					<input id="moduleNum" type="hidden" name="dto.testCaseInfo.moduleNum"/>
					<input id="expResultOld"/>
				</td></tr>
				<!-- <tr >
					<td colspan="5" class="tdtxt" align="center">
						<div id="cUMTxt" align="center" style="color: Blue; padding: 2px"></div>
					</td>
				</tr> -->
				<tr >
					<td class="left_td"><sup>*</sup>类别:</td>
					<td style="width:130px">
						<input id="caseTypeId" class="exui-combobox caseTypeId" name="dto.testCaseInfo.caseTypeId" data-options="
		    			required:true,
		    			valueField:'typeId',
	    				textField:'typeName',
		    			validateOnCreate:false,
		    			editable:false,
		    			prompt:'-请选择-'" style="width:120px"/>
					</td>
					<td class="left_td">基线:</td>
					<td style="width:130px">
						<input id="baseline" class="exui-combobox baseline" name="dto.testCaseInfo.baseline" data-options="
		    			required:false,
		    			validateOnCreate:false,
		    			editable:false,
		    			prompt:'-请选择-'" style="width:120px"/>
					</td>
					<td class="left_td" style="padding-left: 0.5em;"><sup>*</sup>优先级:</td>
					<td style="width:130px">
						<input id="priId" class="exui-combobox priId" name="dto.testCaseInfo.priId" data-options="
		    			required:true,
		    			validateOnCreate:false,
		    			valueField:'typeId',
	    				textField:'typeName',
		    			editable:false,
		    			prompt:'-请选择-'" style="width:120px"/>
					</td>
					<td class="left_td"> &nbsp;&nbsp;<sup>*</sup>执行成本: </td>
					<td style="width:130px">
						<input id="caseWeight" name="dto.testCaseInfo.weight"  class="exui-numberbox" value="2" data-options="min:1,max:10,required:true" style="width: 50px;">
						<font color="orange" style="font-size: 11px;">一成本5分钟</font>
					</td>
				</tr>
				<tr >
					<td class="left_td"><sup>*</sup>用例描述:</td>
					<td class="dataM_left" colspan="8">
						<input id="testCaseDes" name="dto.testCaseInfo.testCaseDes" class="exui-textbox" data-options="required:true" style="width: 729px;">
					</td>
				</tr>
				<tr style="display:none">
					<td class="left_td">前置条件:</td>
					<td colspan="8">
						<input id="prefixCondition" name="dto.testCaseInfo.prefixCondition" class="exui-textbox" style="width: 729px;" data-options="multiline:false,prompt:'前置条件'" />
   			      </td>
				</tr>
				<tr >
					<td class="left_td"><sup>*</sup>过程及数据:</td>
					<td colspan="8" style="background-color: #f7f7f7;">
						<!-- <textarea name="dto.testCaseInfo.operDataRichText" id="operDataRichText"
							cols="50" rows="15" style="width: 100%;">
	   			      </textarea> -->
	   			      <input id="operDataRichText" name="dto.testCaseInfo.operDataRichText" class="exui-textbox" style="width: 729px;height: 140px;" data-options="multiline:true,required:true,prompt:'请填写过程及数据'" />
   			      	</td>
				</tr>
				<tr >
					<td class="left_td"><sup>*</sup>期望结果:</td>
					<td class="dataM_left" colspan="8">
						<input id="expResult" name="dto.testCaseInfo.expResult" class="exui-textbox" style="width: 729px;height: 120px;margin-top: 6px;" data-options="multiline:true,required:true,prompt:'请填写期望结果'" />
   			      </td>
				</tr>
				<tr id="remarkTr">
					<td class="left_td">备注:</td>
					<td colspan="8">
						<input id="testCaseRemark" name="dto.testCaseInfo.remark" class="exui-textbox" style="width: 84%;margin-top: 6px;" data-options="multiline:false,prompt:'备注'" />
						<a id="buttonName" class="exui-linkbutton bntcss" data-options="btnCls:'default',size:'xs'" onclick="showOrHideTagTr()">标签</a>
   			      </td>
				</tr>
				<tr id="tagTr" style="display:none">
					<td class="left_td">标签:</td>
					<td  colspan="8">
						<!-- <input id="tagsValueField" name="dto.testCaseInfo.tags" style="display:none"/>
		  				<ul id="tagsField" style="width:699px"></ul> -->
		  				<div class="multipleSelect selectContainer">
						  <input id="userTagsInput" type="text" class="selectedContent" placeholder="可选择也可输入，标签之间用空格隔开" name="dto.testCaseInfo.tags"/>
						  <div class="dropDown-toggle"  id="tagsSelBtn">选择</div>
						  <ul class="dropDown-menus dsn" id="dropDown-menus">
						   <!-- <li class="items">
						    <span class="isSelectedText">苹果</span>
						    <span class="isSelected"><input type="checkbox"></span>
						   </li>
						   <li class="items">     
						    <span class="isSelectedText">梨</span>
						    <span class="isSelected"><input type="checkbox"></span>
						   </li>
						   <li class="items">
						    <span class="isSelectedText">橘子</span>
						    <span class="isSelected"><input type="checkbox"></span>
						   </li>
						   <li style="text-align: right">
						    <button type="button" class="confirmSelect l-btn btn btn-primary btn-default btn-xs exui-linkbutton">确定</button>
						   </li> -->
						  </ul>
						 </div>
	  			    </td>
				</tr>
				<tr id="execTr" style="display: none;">
					<td class="left_td"><sup>*</sup>执行版本:</td>
					<td>
						<input id="exeVerId" class="exui-combobox" name="dto.exeVerId"  style="width:120px" data-options="
		    			required:false,
		    			validateOnCreate:false,
		    			editable:false,
		    			prompt:'-请选择-'
		    			"/>
					</td>
					<td class="left_td">执行备注: </td>
					<td colspan="5">
							<input id="execRemark" name="dto.remark" class="exui-textbox" style="width: 100%;min-width: 300px;" data-options="multiline:false,prompt:'请填写执行备注'" />
					</td>
				</tr>
			</table>
			</form>
		    <div id="addOrEditFoot" align="right" >
				<div id="operaDiv" class="" style="padding:5px;padding-bottom: 2em;">
					<a id="submit_1" class="exui-linkbutton" data-options="btnCls:'primary',size:'xs'" onclick="editSubmit('1')">修改</a>
					<a class="exui-linkbutton" data-options="btnCls:'default',size:'xs'" onclick="closeEditCaseWin()" style="border: 1px solid #1e7cfb;color: #1e7cfb;">取消</a>
				</div>
				<div id="execDiv" style="padding-bottom: 2em;">
				<a id="previousBtn" href="#" class="exui-linkbutton bntcss" data-options="toggle:true,group:'g1',size:'xs'" onclick="getPreviousCaseData()">上一个</a>
				<a id="nextBtn" href="#" class="exui-linkbutton bntcss" data-options="toggle:true,group:'g1',size:'xs'" onclick="getNextCaseData()">下一个</a>
					<div style="display:inline-block;padding-top:5px;margin-left: 80px;">
					 <a id="previousWrapper">
					   <input type="radio" id="autoPrevious" name="autoLoadCase" style="vertical-align: text-top;" value="previous"/>
					   <label style="color: #1E7CFB;margin-right:5px" for="autoPrevious">自动上一个</label>
					 </a>
					 <a id="nextWrapper">
					   <input type="radio" id="autoNext"  name="autoLoadCase" style="vertical-align: text-top;" value="next"/>
					   <label style="color: #1E7CFB;margin-right:5px" for="autoNext">自动下一个</label>
					 </a>
					</div>
					
					<a id="submit_2" href="#" class="exui-linkbutton bntcss" data-options="toggle:true,group:'g1',size:'xs'" onclick="editSubmit('2')">保存</a>
					<a id="exec_2" href="#" class="exui-linkbutton bntcss" data-options="toggle:true,group:'g1',size:'xs'" onclick="execCaseSubmit('2')">通过</a>
					<a id="exec_3" href="#" class="exui-linkbutton bntcss" data-options="toggle:true,group:'g1',size:'xs'" onclick="execCaseSubmit('3')">未通过</a>
					<a id="exec_5" href="#" class="exui-linkbutton bntcss" data-options="toggle:true,group:'g1',size:'xs'" onclick="execCaseSubmit('5')">阻塞</a>
					<a id="exec_4" href="#" class="exui-linkbutton bntcss" data-options="toggle:true,group:'g1',size:'xs'" onclick="execCaseSubmit('4')">不适用</a>
					<a href="#" class="exui-linkbutton bntcss" data-options="toggle:true,group:'g1',size:'xs'" onclick="closeEditCaseWin()">返回</a>
				</div>
			</div>	
		</div>
		<div id="caseEditFiles" class="tab-pane fade" style="padding-bottom: 1em;">
				<h4 id="editMsg" align="center" style="display: none;">暂无附件信息</h4>
				<form id="editFiles" enctype="multipart/form-data">
		        <div class="form-group">
		            <div class="file-loading">
		                <input id="caseEditFileId" multiple type="file" class="file">
		            </div>
		        </div>
		    </form>
		</div>
		
		<div id="caseHistory" class="tab-pane fade">
		</div>
		<table id="caseHistoryList" class="exui-datagrid" data-options=""></table>
	</div>
	
	<script src="<%=request.getContextPath()%>/itest/js/testCaseManager/editCase.js" type="text/javascript" charset="utf-8"></script>
