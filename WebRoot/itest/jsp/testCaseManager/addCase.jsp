<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<style type="text/css">
       /* .container{
  margin: 20px auto;
  padding:0 15px;
  width: 50%; 
  height:300px;
  box-sizing: border-box;  
 }
 .text-container{
  display: inline-block;
  float:left;
  width: 15%;
  height: 32px;
  line-height: 32px;
  box-sizing: border-box;
 } */
 .selectContainer{
  width: 70%;
  /* height:200px; */
  float:left;
  position: relative;
  padding:0;
  margin:0;
  box-sizing: border-box;
 }
 .selectedContent{
  width:85%;
  height: 32px;
  float:left;   
 }
 .dropDown-toggle{
  width:14%;
  height:31px;
  line-height: 31px;
  text-align: center;
  border: 1px solid silver;
  border-left:none;
  float:left;
  padding:0;
  margin:0;
  box-sizing: border-box;
  cursor: pointer;
 }
 .dropDown-menus{
  margin:0;
  padding:0 15px 10px;
  width:100%;
  border:1px solid silver;
  border-top: none;
  box-sizing: border-box;
  list-style: none;
  position: absolute;
  top:31px;
  right:0;
  background-color: white;
  z-index:2000;
  height: 100px;
  overflow-y:auto;
 }
 .items{
  margin-top:8px;
  padding: 2px;
  cursor: pointer;
 }
 .items:hover{
  background: #ddd;
 }
 .isSelectedText{
  display: inline-block;
  width:90%;
 }
 .dsn{
  display: none;
 }
 .form-table tr {
    height: 2.5em;
}
    </style>
	<form id="addOrEditCaseForm" method="post">
		<table class="form-table">
			<tr class="hidden"><td>
				<input type="hidden" id="mdPath" name="mdPath" value="" />
				<input id="task_Id" type="hidden" name="dto.testCaseInfo.taskId"/>
				<input id="moduleId" type="hidden" name="dto.testCaseInfo.moduleId"/>
				<input id="createrId" type="hidden" name="dto.testCaseInfo.createrId"/>
				<input id="testCaseId" type="hidden" name="dto.testCaseInfo.testCaseId"/>
				<input id="isReleased" type="hidden" name="dto.testCaseInfo.isReleased"/>
				<input id="creatdate" type="hidden" name="dto.testCaseInfo.creatdate"/>
				<input id="attachUrl" type="hidden" name="dto.testCaseInfo.attachUrl"/>
				<input id="auditId" type="hidden" name="dto.testCaseInfo.auditId"/>
				<input id="testStatus" type="hidden" name="dto.testCaseInfo.testStatus"/>
				<input id="testData" type="hidden" name="dto.testCaseInfo.testData"/>
				<input id="moduleNum" type="hidden" name="dto.testCaseInfo.moduleNum"/>
				<input id="expResultOld"/>
			</td></tr>
			<!-- <tr >
				<td colspan="5" class="tdtxt" align="center">
					<div id="cUMTxt" align="center" style="color: Blue; padding: 2px"></div>
				</td>
			</tr> -->
			<tr >
				<td class="left_td"><sup>*</sup>类别:</td>
				<td style="width:130px">
					<input id="caseTypeId" class="exui-combobox caseTypeId" name="dto.testCaseInfo.caseTypeId" data-options="
	    			required:true,
	    			valueField:'typeId',
    				textField:'typeName',
	    			validateOnCreate:false,
	    			editable:false,
	    			prompt:'-请选择-'" style="width:120px"/>
				</td>
				<td class="left_td">基线:</td>
				<td style="width:130px">
					<input id="baseline" class="exui-combobox baseline" name="dto.testCaseInfo.baseline" data-options="
	    			required:false,
	    			validateOnCreate:false,
	    			prompt:'-请选择-'" style="width:120px"/>
				</td>
				<td class="left_td" style="padding-left: 0.5em;"><sup>*</sup>优先级:</td>
				<td style="width:130px">
					<input id="priId" class="exui-combobox priId" name="dto.testCaseInfo.priId" data-options="
	    			required:true,
	    			validateOnCreate:false,
	    			valueField:'typeId',
    				textField:'typeName',
	    			editable:false,
	    			prompt:'-请选择-'" style="width:120px"/>
				</td>
				<td class="left_td"> &nbsp;&nbsp;<sup>*</sup>成本: </td>
				<td style="width:130px">
					<input id="caseWeight" name="dto.testCaseInfo.weight"  class="exui-numberbox" value="1" data-options="min:1,max:10,required:true" style="width: 50px;">
					<font color="orange" style="font-size: 11px;">一单位5分钟</font>
				</td>
			</tr>
			<tr >
				<td class="left_td"><sup>*</sup>用例描述:</td>
				<td class="dataM_left" colspan="8">
					<input id="testCaseDes" name="dto.testCaseInfo.testCaseDes" class="exui-textbox" data-options="required:true" style="width: 100%;">
				</td>
			</tr>
			<tr style="display:none">
				<td class="left_td">前置条件:</td>
				<td  colspan="8">
					<input id="prefixCondition" name="dto.testCaseInfo.prefixCondition" class="exui-textbox" style="width: 100%;" data-options="multiline:false,prompt:'前置条件'" />
  			      </td>
			</tr>
			<tr >
				<td class="left_td"><sup>*</sup>过程及数据:</td>
				<td  colspan="8" style="background-color: #f7f7f7;">
					<!-- <textarea name="dto.testCaseInfo.operDataRichText" id="operDataRichText"
						cols="50" rows="15" style="width: 100%;">
   			      </textarea> -->
   			      <input id="operDataRichText" name="dto.testCaseInfo.operDataRichText" class="exui-textbox" style="width: 100%;height: 160px;margin-bottom:3px" data-options="multiline:true,required:true,prompt:'请填写过程及数据'" />
  			      	</td>
			</tr>
			<tr >
				<td class="left_td"><sup>*</sup>期望结果:</td>
				<td class="dataM_left"  colspan="8">
					<input id="expResult" name="dto.testCaseInfo.expResult" class="exui-textbox" style="width: 100%;height: 120px;margin-top: 6px;" data-options="multiline:true,required:true,prompt:'请填写期望结果'" />
  			      </td>
			</tr>
			<tr id="remarkTr">
				<td class="left_td">备注:</td>
				<td  colspan="8">
					<input id="testCaseRemark" name="dto.testCaseInfo.remark" class="exui-textbox" style="width: 87%;margin-top: 6px;" data-options="multiline:false,prompt:'备注'" />
					<a class="exui-linkbutton bntcss" data-options="btnCls:'default',size:'xs'" onclick="showOrHideTagTr()">维护标签</a>
  			      </td>
			</tr>
			<tr id="tagTr" style="display:none">
				<td class="left_td">标签:</td>
				<td  colspan="8">
					<!-- <input id="tagsValueField" name="dto.testCaseInfo.tags" style="display:none"/>
	  				<ul id="tagsField" style="width:699px"></ul> -->
	  				<div class="multipleSelect selectContainer">
					  <input type="text" class="selectedContent" placeholder="可选择也可输入，标签之间用空格隔开" name="dto.testCaseInfo.tags"/>
					  <div class="dropDown-toggle">选择</div>
					  <ul class="dropDown-menus dsn" id="dropDown-menus">
					  </ul>
					 </div>
  			    </td>
			</tr>
			<tr id="execTr" style="display: none;">
				<td class="left_td"><sup>*</sup>执行版本:</td>
				<td>
					<input id="exeVerId" class="exui-combobox" name="dto.exeVerId"  style="width:120px" data-options="
	    			required:false,
	    			validateOnCreate:false,
	    			prompt:'-请选择-'
	    			"/>
				</td>
				<td class="left_td"> &nbsp;&nbsp;执行备注: </td>
				<td colspan="3">
						<input id="execRemark" name="dto.remark" class="exui-textbox" style="width: 100%;min-width: 300px;" data-options="multiline:false,prompt:'请填写执行备注'" />
				</td>
			</tr>
		</table>
	</form>
	<form enctype="multipart/form-data">
        <div class="form-group">
            <div class="file-loading">
                <input id="caseUploadFileId" multiple type="file" class="file">
            </div>
        </div>
    </form>
    <div id="addOrEditFoot" align="right" >
		<div id="operaDiv" class="">
			<a id="submit_1" class="exui-linkbutton" data-options="btnCls:'primary',size:'xs'" onclick="submitForm('1')">保存</a>
			<a id="submit_2" class="exui-linkbutton" data-options="btnCls:'primary',size:'xs'" onclick="submitForm('2')">保存并继续</a>
			<a id="cancleBnt" class="exui-linkbutton" data-options="btnCls:'default',size:'xs'" onclick="closeCaseWin()" style="border: 1px solid #1e7cfb;color: #1e7cfb;">取消</a>
		</div>
	</div>
	
	<script src="<%=request.getContextPath()%>/itest/js/testCaseManager/addCase.js" type="text/javascript" charset="utf-8"></script>