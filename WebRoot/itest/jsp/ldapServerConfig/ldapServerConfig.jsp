<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<style>
 
th{
 	font-weight:400;
}

.tools button,.tools button:hover{
  border: 1px solid #1E7CFB;
    color: #1E7CFB;
}

tr>th{
  font-size: 14px;
}

.indent-60{
   text-indent: 45px;
}
.datagrid-header {
	height: 40px;
}
.datagrid-htable{
	height: 40px;
}
.datagrid-body td{
    padding: 11px 0;
} 

.searchable-select{
  min-width: 130px;
}
.has-error .form-control {
    border-color: #aaa;
    }
</style>

<div class="newWrapper" style="padding:15px 15px 0 15px;height:auto" >     
	<!--top tools area--> 
	<div id="ldapServerConfigSear">
		<div class="tools" id="ldapServerConfigTool">
			<div class="input-field" style="width: 240px;">
			    <span style="width: 100px;color:#1E7CFB">快速查询：</span>
				<input class="form-control indent-4-5" id="ldapServerConfigQuery" style="border: 1px solid #1E7CFB;" placeholder="BASEDN路径+回车键"/>
			</div>
			
			<button type="button" class="btn btn-default" id="ldapServerConfigReset" onclick="ldapServerConfigReset()"><i class="glyphicon glyphicon-repeat"></i>重置</button>
		    <button type="button" class="btn btn-default" id="ldapServerConfigAdd" onclick="ldapServerConfigAdd()" schkUrl=""><i class="glyphicon glyphicon-plus"></i>增加</button>
			<button type="button" class="btn btn-default" id="ldapServerConfigEdit" onclick="ldapServerConfigEdit()"  schkUrl=""><i class="glyphicon glyphicon-pencil"></i>修改</button>
			<button type="button" class="btn btn-default" id="ldapServerConfigDel" onclick="ldapServerConfigDel()" schkUrl=""><i class="glyphicon glyphicon-remove"></i>删除</button>
		</div><!--/.top tools area-->
	
		<table id="ldapServerConfigList" style="width:100%;"></table>
	</div>
</div>
	
	<!-- ldap服务配置模态窗 -->
	<div id="ldapConfigInfoWin" class="exui-window" style="display:none;" data-options="
		modal:true,
		width: 500,
		minimizable:false,
		maximizable:false,
		resizable:false,
		closed:true">
		<form id="ldapConfigInfoForm" class="form-horizontal" method="post" enctype="multipart/form-data" role="form">
			<input id="ldapId" name="ldapConfigInfoDto.ldapConfigInfo.id" type="hidden"/>
			<input id="ldapEnable" name="ldapConfigInfoDto.ldapConfigInfo.enable" type="hidden"/>
			<table class="form-table">
				<tr>
					<th>IP地址<sup>*</sup>：</th>
		    		<td><input id="ldapServerIp" class="exui-textbox" name="ldapConfigInfoDto.ldapConfigInfo.serverIp" data-options="required:true,validateOnCreate:false" style="width:330px"/></td>
		    	</tr>
		    	
		    	<tr>
		    	  <th>端口<sup>*</sup>：</th>
		    		<td><input id="ldapPort" class="exui-textbox" name="ldapConfigInfoDto.ldapConfigInfo.port" data-options="required:true,validateOnCreate:false" style="width:330px"/></td>
		    	</tr>
		    	
		    	<tr>
		    	  <th>baseDn路径<sup>*</sup>：</th>
		    		<td><input id="ldapBaseDn" class="exui-textbox" name="ldapConfigInfoDto.ldapConfigInfo.baseDn" data-options="required:true,validateOnCreate:false" style="width:330px"/></td>
		    	</tr>
		    	
		    	<tr>
		    	  <th>密码<sup>*</sup>：</th>
		    		<td><input id="bindPassword" class="exui-textbox" name="ldapConfigInfoDto.ldapConfigInfo.bindPassword" data-options="required:true,validateOnCreate:false" style="width:330px"/></td>
		    	</tr>
		    	
		    	<!-- <tr>
		    	  <th>启禁用：</th>
		    		<td>
		    			<input class="exui-combobox enableLdap" id="enableLd" name="ldapConfigInfoDto.ldapConfigInfo.enable" data-options="
		    			validateOnCreate:false,
		    			valueField:'id',
		    			textField:'text',
		    			editable:false,
		    			prompt:'-请选择-',
		    			value:'0',
		    			data:[{id: 0 ,text:'禁用',selected:true},{id: 1 ,text:'启用'}]" style="width:330px"/>
			    	</td>
		    	</tr> -->
		    	
		    	<tr>
		    	  <th>备注：</th>
		    		<td><input id="ldapNote" class="exui-textbox" name="ldapConfigInfoDto.ldapConfigInfo.note" data-options="" style="width:330px"/></td>
		    	</tr>
			</table>
		</form>
		<footer style="padding:5px;text-align:right">
			<a class="exui-linkbutton" data-options="btnCls:'primary',size:'xs'" onclick="saveLdapConfigInfo()">保存</a>
			<a class="exui-linkbutton" data-options="btnCls:'default',size:'xs'" onclick="cancleConfigInfo()"  style="border: 1px solid #1e7cfb;color: #1e7cfb;">取消</a>
		</footer>
	</div>
	
<script src="<%=request.getContextPath()%>/itest/js/ldapServerConfig/ldapServerConfig.js" type="text/javascript" charset="utf-8"></script>
