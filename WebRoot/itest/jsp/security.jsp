<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="cn.com.codes.common.util.StringUtils"%>
<%@page import="cn.com.codes.framework.security.filter.SecurityContextHolder"%>
<%@page import="cn.com.codes.framework.security.filter.SecurityContext"%>
<%@page import="cn.com.codes.framework.security.Visit"%>  
<%@page import="cn.com.codes.framework.security.VisitUser"%>
<%@page import="java.util.Set" %>
<%@page import="java.util.Iterator" %>
<%@page import="java.lang.String" %>
<%@ page import="java.util.List"%>
<%@ page import="java.util.Map"%> 
<script type="text/javascript">
	var privilegeMap = new Object();
	var userPowerList=new Array();
	<%
	SecurityContext sc = SecurityContextHolder.getContext();
	Visit visit = sc.getVisit();
	VisitUser user =  null;
	if(visit!=null){
		user = visit.getUserInfo(VisitUser.class);
	}
	 
	if(user!=null){
		Set<String> userPrivileges   =  user.getPrivilege();
		if(userPrivileges!=null&&!userPrivileges.isEmpty()){
			Iterator<String> it = userPrivileges.iterator();
			while(it.hasNext()){
				String url = it.next();
				%>
				privilegeMap["<%=url%>"] ="1" ; 
				<%
			}
		}
	}

	%> 

	
</script>



