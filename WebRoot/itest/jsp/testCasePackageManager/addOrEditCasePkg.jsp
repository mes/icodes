<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<style>
.panel-default {
    border-color: #cccccc;
}

.exportTestcaseTips{
 font-size:10px;
 color:#2b2727;
}
.exportTestcaseTitle{
 width:100%;
 color:#777;
 border-bottom:1px dashed #4845458f;
 padding-bottom:10px
}
.btnTools{
    margin-bottom: 15px;
    display: flex;
    justify-content: space-between;
}

.btnTools .btn, .btnTools .btn:hover {
    border: 1px solid #1E7CFB;
    color: #1E7CFB;
}
.form-table tr {
    height: 2.5em;
}
</style>
<!-- 新增/修改单井模态窗开始 -->
<div id="addOrEditPkgWin">
	<table class="form-table" style="width:100%">
		<form id="addOrEditPkgForm" method="post">
			<tr class="hidden">
			  <td>
			    <input class="exui-textbox"  name="dto.testCasePackage.packageId" id="packageId"/>
			    <input class="exui-textbox"  name="dto.testCasePackage.createTime" id="createTime"/>
			    <input class="exui-textbox"  name="dto.testCasePackage.updateTime" id="updateTime"/>  
			    <input class="exui-textbox"  name="dto.testCasePackage.createrId" id="createrId"/>  	
			    <input class="exui-textbox"  name="dto.otherCaseModuleNum" id="testCaseModuleNums"/>  			    
			  </td>
			</tr>
			<tr>
				<th style="width:26%"><sup>*</sup>测试包名称：</th>
	    		<td><input class="exui-textbox" name="dto.testCasePackage.packageName" id="packageName" style="width:75%"/></td>
	    	</tr>
	    	<tr>
	    	<th  style="width:25%"><sup>*</sup>执行人：</th>
	    		<td><select id="executor" class="exui-combobox executor"  data-options="
	    			valueField:'id',
	    			textField:'name',
	    			multiple:true,
	    			editable:false,
	    			prompt:'-请选择-'" style="width:75%"></select>	<span class="multPkgPeopleSel" onclick="showSeletctPeopleWindow('executor');" title="选择人员" ></span>
	    			
	    		</td>
	    	</tr>
	    	<tr>
	    		<th  style="width:25%">执行环境：</th>
	    		<td>
	    		  <textarea class="exui-textbox" name="dto.testCasePackage.execEnvironment" id="execEnvironment"  style="width:75%"></textarea>
	    		</td>
	    	</tr>
	    	<tr>
	    	   <th style="width:25%"><sup>*</sup>执行版本： </th>
				<td>
					<input id="execVersion" class="exui-combobox" name="dto.testCasePackage.execVersion"  style="width:75%" data-options="
	    			required:false,
	    			validateOnCreate:false,
					editable:false,
	    			prompt:'-请选择-'
	    			"/>
				</td>
	    	</tr>
	    	<tr>
	    		<th  style="width:25%">备注：</th>
	    		<td>
	    		  <textarea class="exui-textbox" name="dto.testCasePackage.remark"  style="width:75%"></textarea>
	    		</td>
	    	</tr>
	    	<tr>
	    		<th  style="width:25%"><sup>*</sup>预计开始时间：</th>
	    		<td>
	    		  <input class="exui-datebox" name="dto.testCasePackage.expectedStartTime" id="expectedStartTime" style="width:75%" data-options="validateOnCreate:false,prompt:'-请选择-',editable:false"
></input>
	    		</td>
	    	</tr>
	    	<tr>
	    		<th  style="width:25%"><sup>*</sup>预计结束时间：</th>
	    		<td>
	    		  <input class="exui-datebox" name="dto.testCasePackage.expectedEndTime" id="expectedEndTime" style="width:75%" data-options="validateOnCreate:false,prompt:'-请选择-',editable:false"
></input>
	    		</td>
	    	</tr>
	    	<tr>
	    		<th  style="width:25%">实际开始时间：</th>
	    		<td>
	    		  <input class="exui-datebox" name="dto.testCasePackage.actualStartTime" id="actualStartTime" style="width:75%" data-options="validateOnCreate:false,prompt:'-请选择-',editable:false"
></input>
	    		</td>
	    	</tr>
	    	<tr id="tipsline">
	    	  <td colspan="2">
	    	    <div class="exportTestcaseTitle">分配用例到测试包<span class="exportTestcaseTips">&nbsp;&nbsp;&nbsp;(可选择任意一种方式)</span></div>
	    	  </td>
	    	</tr>
	    	<tr id="exportOtherPkgs">
	    	  <th  style="width:25%">从其他用例包选择：</th>
	    		<td>
	    		<select id="testPkgs" data-options="
		    			valueField:'id',
		    			textField:'name',
		    			multiple:true,
		    			editable:false,
		    			prompt:'-请选择-'" style="width:75%">
	    			</select>
	    		</td>
	    	</tr>
	    	<tr id="exportOtherModule">
	    	  <th  style="width:29%">从测试需求中选择：</th>
	    		<td>
	    		 <span id="moduleNames" style="vertical-align: sub;"></span>    
	    		  <a class="exui-linkbutton" data-options="btnCls:'primary',size:'xs'" id="showTestCaseTreeBtn">点击选择</a>
	    		</td>
	    	</tr>
	    	<tr id="tagsFilter">
				<th style="width:26%">包含标签：</th>
	    		<td><input class="exui-textbox" id="tagName" style="width:75%" data-options="required:false,prompt:'填写标签，多个标签用空格隔开，最多三个'"/></td>
	    	</tr>
	    </form>
	</table>
</div>
<div id="addOrEditPkgFooter" align="right" style="padding:5px;    margin-top: 20px;">
	<a class="exui-linkbutton" data-options="btnCls:'primary',size:'xs'" id="submitPkgBtn">保存</a>
	<a class="exui-linkbutton"  data-options="btnCls:'default',size:'xs'" style=" border: 1px solid #1E7CFB;color: #1E7CFB;" id="closePkgWinBtn">取消</a>
</div>

<div id="testCaseTreeWin" class="exui-window" style="display:none;" data-options="
	modal:true,
	width: 480,
	height:500,
	top:90,
	minimizable:false,
	maximizable:false,
	closed:true">
	<div class="btnTools">
	   <a class="btn btn-default" onclick="expandOrCollapseAll()">展开/折叠</a>
	  <button class="btn btn-default" onclick="handleConfirmModule()">确认</button>
	</div>
   <div>
     	<div id="testCaseDetailTree"></div>
   </div>
</div>
<!-- 新增/修改单井模态窗结束 -->
<script src="<%=request.getContextPath()%>/itest/js/testCasePackageMananger/addOrEditCasePkg.js" type="text/javascript" charset="utf-8"></script>
