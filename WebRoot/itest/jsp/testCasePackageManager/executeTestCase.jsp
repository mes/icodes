<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<style>
  .tools button:nth-child(2), .tools button:nth-child(3), .tools button:nth-child(2):hover ,.tools button:nth-child(3):hover {
    border: 1px solid #1E7CFB;
    color: #1E7CFB;
   }
   
   .testcaseResultWrapper{
	   display:inline-block;
	   border: 1px dashed #1e90fc;
	   height: 35px;
	   line-height: 32px;
	   vertical-align: bottom;
	   margin-left: 30px;
   }
   
   .testCaseResultCount{
      font-size: 1.5rem;
      font-weight: bold;
      margin-right: 20px;
   }
	    .messagecss {
		    position: absolute;
		    top: -0.5em;
		    right: -0.5em;
		}
		.fileCount {
		    display: none;
		    width: 18px;
		    height: 18px;
		    text-align: center;
		    position: absolute;
		    top: -0.8em;
		    right: -3em;
		    left: 9em;
		    color: #5684fe;
			border: 1px solid #5684fe;
		    border-radius: 18px;
		    font-size: 11px;
		}
 #executeTestCaseDlg .datagrid-wrap{
	  height:440px!important
	}
	.form-table tr {
	    height: 2.5em;
	}
</style>

<div>
   <div id="execBtnTool" class="tools" >
		<div class="input-field" style="width: 90px;">
	    <select id="exeStatus" class="form-control chzn-select" onchange="searchExeStatus()">
	    	<option value="-1">全部</option>
<!-- 	    	<option value="0">待审核</option>
 -->	    	<option value="1">未测试</option>
	    	<option value="2">通过</option>
	    	<option value="3">不通过</option>
	    	<option value="4">不适用</option>
	    	<option value="5">阻塞</option>
<!-- 	    	<option value="6">待修正</option>
 -->	    </select>
	</div>
	<!-- <button type="button" class="btn btn-default" onclick="executeCaseWin()"><i class="glyphicon glyphicon-asterisk"></i>执行</button> -->
    <button type="button" class="btn btn-default" onclick="delSelectedTestCase()" ><i class="glyphicon glyphicon glyphicon-remove"></i>移除用例</button>
    <a style="padding:6.5px 15px;" href="#" type="button" class="btn btn-default bntcss hoverBu" onclick="exportCase(this);" title="导出离线执行"><i class="glyphicon glyphicon-arrow-down"></i>离线执行</a>
    <button type="button" class="btn btn-default bntcss hoverBu" id ="uploadCase" onclick="syncOffline();" title="同步线执行"><i class="glyphicon glyphicon-arrow-up"></i>同步离线执行</button>
    <button type="button" class="btn btn-default bntcss hoverBu" onclick="closeExecWin()" style="margin-left:1rem"><i class="glyphicon glyphicon-off"></i>关闭</button>
    <div class="testcaseResultWrapper">    
    <ul style="display: flex;list-style: none;margin:0">
      <li class="testCaseResultCount" style="color:#a3d3fe;"><span >总用例数：</span><span id="execAllCount">0</span></li>
      <li class="testCaseResultCount" style="color: #71CD71;"><span >通过数：</span><span id="execPassCount">0</span></li>
      <li class="testCaseResultCount" style="color: #ED8282;"><span >未通过数：</span><span id="execFailedCount">0</span></li>
      <li class="testCaseResultCount" style="color: #EEAA5A;"><span >阻塞数：</span><span id="execBlockCount">0</span></li>
      <li class="testCaseResultCount" style="color: #B78EEA;"><span >不适用数：</span><span id="execInvalidCount">0</span></li>
      <li class="testCaseResultCount" style="color: #B7AB9C;"><span >未测试数：</span><span id="execNoTestCount">0</span></li>
    </ul>
    </div>
  
  </div><!--/.top tools area-->
  <div style="width:100%;display:inline-block">
    <table id="executeTestCaseList"  title="" style="width:100%;height: auto;" data-options="fitColumns: true,
	rownumbers: false,
	singleSelect: true,
	pagination: true,
	pageNumber: 1,
	pageSize: 10,
	layout:['list','first','prev','manual','next','last','refresh','info']
	"></table>
  </div>
  <div style="border:1px solid #eee;margin:10px 0;clear:both"></div>
</div>
	<!-- 从excel导入测试用例模态窗 -->
<div id="syncOfflineWin" class="exui-window" style="display:none;" data-options="
	modal:true,
	width: 450,
	minimizable:false,
	maximizable:false,
	resizable:false,
	closed:true">
	<form id="syncOfflineForm" class="form-horizontal" method="post" enctype="multipart/form-data" role="form">
		<div class="file-box">
			<!-- <input id="excelField" class="form-control pull-left" style="max-width: 380px;" readonly type="text">
			<button class="pull-left" type="button">浏览...</button>
			<input id="importFile" name="dto.importFile" style="width:5em;" size="15" onchange="fileChange(this)" type="file" accept=".xls,.xlsx"> -->
			<input id="syncOfflineImportFile" accept=".xls,.xlsx,.xmind" name="dto.importFile" type="file">
			<br/><span id="tips" style="color:red">&nbsp;&nbsp;&nbsp;excel导入时，请按照模板要求导入数据，否则导入将会不成功！</span>
		</div>
	</form>
	<footer style="padding:5px;text-align:right">
		<a class="exui-linkbutton" data-options="btnCls:'primary',size:'xs'" onclick="exceSyncOffline()">确认导入</a>
		<a class="exui-linkbutton" data-options="btnCls:'default',size:'xs'" onclick="closeSyncOfflineWin()"  style="border: 1px solid #1e7cfb;color: #1e7cfb;">取消</a>
	</footer>
</div>
<script type="text/javascript" src="<%=request.getContextPath()%>/itest/js/testCasePackageMananger/executeTestCase.js" charset="utf-8"></script>
