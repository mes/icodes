<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<style>
 th{
  font-size: 13px;
    text-align: center;
 }
 
 tr td{
   text-align:center;
 }
</style>

<div>
     <div style="padding: 30px 0 0 30px;">
    <label>版本：</label>
	 <select class="exui-combobox" id="bugExistDayVer"  style="width:170px"/></select>	 
	</div>
	<div style="display:none" id="nullBugExistDayData" >
	  <h3 style="margin:150px 0 150px 150px">暂无此项目报表数据</h3>
	</div>
<div id="bugExistDayStat">
   <!-- ehcarts -->
    <div id="testerDayEcharts" style="margin:50px auto 20px;width: 700px; height:400px;"></div>
   <!-- 表格 -->
   
   <table id="testerDayTable" class="table table-bordered" style="width:700px;margin: 20px auto">
   <thead style="text-align:center;background-color:#dce9eb"><tr id="testerDayThead"></tr></thead>
   <tbody id="testerDayTbody"></tbody>
    </table>
</div>

</div>
<script type="text/javascript" src="<%=request.getContextPath()%>/itest/js/analysisManage/bugExistDayStat.js"></script>
