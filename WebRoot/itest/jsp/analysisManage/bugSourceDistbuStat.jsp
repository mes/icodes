<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<style>
#bugSourceDisbuStat th{
  font-size: 13px;
    text-align: center;
 }
 
#bugSourceDisbuStat tr td{
   text-align:center;
 }
 
#bugSourceDisbuStat  div.tips{
   width:700px;
   margin:22px auto;
 }
 
#bugSourceDisbuStat p{
   font-size:15px;
   color:blue;
   font-weight:bold
 }
</style>

<div >   
 <div style="padding: 30px 0 0 30px;">
    <label>版本：</label>
	 <select class="exui-combobox" id="bugSourceVer"  style="width:170px"/></select>	 
	</div>
<div id="bugSourceDisbuStat">
   <!-- ehcarts -->
     <div id="bugSourceStatEcharts" style="margin:50px auto 0px;width: 700px; height:300px;"></div>
    <div id="bugSourceStatPie" style="margin:0px auto 20px;width: 700px; height:400px;"></div>
    
   <!-- 表格 -->
   
   <table id="bugSourceStatTable" class="table table-bordered" style="width:700px;margin: 20px auto">
   <thead style="text-align:center;background-color:#dce9eb">
     <tr>
       <th>来源</th>
       <th>BUG数</th>
     </tr>
   </thead>
   <tbody id="bugSourceStatTbody"></tbody>
    </table>
    <div class="tips" style="margin:25px auto">
     <p >状态为:撤销,无效 ,重复 ,非错 ,关闭/撤销 ,修正/描述不当的BUG不计算在内</p>
   </div>
   </div>
</div>

<script type="text/javascript" src="<%=request.getContextPath()%>/itest/js/analysisManage/bugSourceDistbuStat.js"></script>
