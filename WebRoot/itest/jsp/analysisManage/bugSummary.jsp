<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<style>
#bugSummaryWrapper th{
  font-size: 13px;
    text-align: center;
 }
 
#bugSummaryWrapper tr td,#bugSummaryWrapper caption{
   text-align:center;
 }
 
#bugSummaryWrapper  div.tips{
   width:700px;
   margin:22px auto;
 }
 
#bugSummaryWrapper p{
   font-size:15px;
   color:blue;
   font-weight:bold
 }
 
#bugSummaryWrapper caption{
   font-size:15px;
   color:blue;
   font-weight:bold
 }
 
   .tools button,.tools button:hover{
  border: 1px solid #1E7CFB;
    color: #1E7CFB;
}

.tools button:first-of-type{
  background: #1E7CFB;
    border: 1px solid #1E7CFB;
    color: #ffffff;
}
.datebox, .datebox .textbox-text {
  width: 120px!important;
}
#proVersion_summary~.textbox {
   width: 120px!important;
}
</style>

<div id="bugSummaryWrapper">   
	<div class="tools" style="width:100%;margin:20px auto;text-align:center">  
			<span>开始:</span>
		<div class="input-field" style="width:130px;height:29px"> 
		<input class="exui-datebox" id="startDate" editable="false" placeholder="请输入登陆账号"/>
		</div>
		<span>结束:</span>
	    
		<div class="input-field" style="width:130px;height:29px"> 
			<input class="exui-datebox" id="endDate" editable="false" placeholder="请输入登陆账号"/>
		</div>
	  <span>版本:</span>
	 <div class="input-field" style="width:130px;height:29px"> 
	   <input id="proVersion_summary" class="exui-combobox" editable="false" data-options="
	            required:false,
	            validateOnCreate:false, 
	            prompt:'-请选择版本-'
	     "/>
	  </div>
	 <input type="checkbox"  id="includeChange01" value="1" />&nbsp;包含修改 &nbsp;
	 
		<button type="button" style="margin-top:7px;padding:5px 10px" class="btn btn-default" id="viewReport"><!-- <i class="glyphicon glyphicon-search"></i> -->重查</button>
		<button type="button" style="margin-top:7px;padding:5px 10px" class="btn btn-default" id="resetInp"><!-- <i class="glyphicon glyphicon-pencil"></i> -->重置</button>
	</div>
	<div >
	   <table id="bugNewFixCloseSummaryTable" class="table table-bordered" style="width:700px;margin: 20px auto">
	   <caption>时间段内新增、修改和关闭BUG概况</caption>
	   <thead style="background-color:#dce9eb" id="bugNewFixCloseSummaryThead">
	   </thead>
	   <tbody id="bugNewFixCloseSummaryTbody"></tbody>
	    </table>
  </div>
  <!--测试人员名下BUG汇总-->
  <div > 
	   <table id="testerBugsTable" class="table table-bordered" style="width:700px;margin: 20px auto">
	   <caption>测试人员名下BUG汇总</caption>
	   <thead style="background-color:#dce9eb" >
	   <tr> 
	      <td style='text-align:center;'>测试人员</td>
	      <td style='text-align:center;'>BUG数</td>
	      <td style='text-align:center;'>BUG占比</td>
	      <td style='text-align:center;'>无效BUG数</td>
	      <td style='text-align:center;'>个人无效BUG占比</td>
	      <td style='text-align:center;'>费解BUG数</td>
	      <td style='text-align:center;'>个人费解BUG占比</td>
	    </tr>
	   </thead>
	   <tbody id="testerBugsTbody"></tbody>
	    </table>
	<div class="tips">
     <p style="color:#000000">无效BUG指状态为：撤销、修正/描述不当、重复、无效、非错、关闭/撤销的BUG</p>
   </div>
  </div>
  <!--  -->
  <div >
	   <table id="bugBeforeOpenSummaryTable" class="table table-bordered" style="width:700px;margin: 20px auto">
	   <caption>测试人员BUG状态分布情况</caption>
	   <thead style="background-color:#dce9eb">
	    <tr> 
	      <td style='text-align:center;'> 测试人员</td>
	      <td style='text-align:center;'> BUG状态</td>
	      <td style='text-align:center;'>BUG数</td>
	    </tr>
	   </thead>
	   <tbody id="bugBeforeOpenSummaryTbody"></tbody>
	    </table>
  </div>
    <!--  -->
  <div >
	   <table id="bugLevelTable" class="table table-bordered" style="width:700px;margin: 20px auto">
	   <caption>测试人员BUG等级分布情况</caption>
	   <thead style="background-color:#dce9eb">
	    <tr> 
	      <td style='text-align:center;'> 测试人员</td>
	      <td style='text-align:center;'> BUG等级</td>
	      <td style='text-align:center;'>BUG数</td>
	    </tr>
	   </thead>
	   <tbody id="bugLevelTbody"></tbody>
	    </table>
  </div>
    <!--  -->
  <div >
	   <table id="exeCaseTable" class="table table-bordered" style="width:700px;margin: 20px auto">
	   <caption>测试人员执行用例情况</caption>
	   <thead style="background-color:#dce9eb">
	    <tr> 
	      <td style='text-align:center;'> 测试人员</td>
	      <td style='text-align:center;'> 执行通过</td>
	      <td style='text-align:center;'> 执行未通过</td>
	      <td style='text-align:center;'> 执行阻塞</td>
	      <td style='text-align:center;'> 执行成本</td>
	    </tr>
	   </thead>
	   <tbody id="exeCaseTbody"></tbody>
	    </table>
  </div>
    <!--  -->
  <div >
	   <table id="writeCaseTable" class="table table-bordered" style="width:700px;margin: 20px auto">
	   <caption>测试人员编写用例情况</caption>
	   <thead style="background-color:#dce9eb">
	    <tr> 
	      <td style='text-align:center;'> 测试人员</td>
	      <td style='text-align:center;'>编写用例数</td> 
	    </tr>
	   </thead>
	   <tbody id="writeCaseTbody"></tbody>
	    </table>
  </div>
</div>

<script type="text/javascript" src="<%=request.getContextPath()%>/itest/js/analysisManage/bugSummary.js"></script>
