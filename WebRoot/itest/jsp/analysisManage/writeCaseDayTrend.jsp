<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<style>
 .tools button,.tools button:hover{
  border: 1px solid #1E7CFB;
    color: #1E7CFB;
}

.tools button:first-of-type{
  background: #1E7CFB;
    border: 1px solid #1E7CFB;
    color: #ffffff;
}
</style>

<div>
   
   
<div class="tools" style="margin:20px auto;width:90%">  
	<lable>开始:</lable>
	<div class="input-field" style="width:170px;height:29px"> 
		<input class="exui-datebox" id="startDate" editable="false" placeholder="请输入登陆账号"/>
	</div>
	<lable>结束:</lable>
	    
	<div class="input-field" style="width:170px;height:29px"> 
		<input class="exui-datebox" id="endDate" editable="false" placeholder="请输入登陆账号"/>
	</div>
 	<input type="checkbox"  id="includeChange" value="1" />&nbsp;包含修改 &nbsp;
	<button id="viewReport" type="button" style="margin-top:7px;padding:5px 10px" class="btn btn-default" ><!-- <i class="glyphicon glyphicon-search"></i> -->重查</button>
	<button id="resetInp" type="button" style="margin-top:7px;padding:5px 10px" class="btn btn-default" ><!-- <i class="glyphicon glyphicon-pencil"></i> -->重置</button>

</div>
<div style="margin-left:100px;display:none" id="nullData" >
<h3 style="margin:150px 0 150px 150px">暂无此项目报表数据</h3>
</div>
  <div id="mainContent">
   <!-- ehcarts -->
    <div id="main" style="margin:50px auto;width: 700px; height:400px;"></div>
    <!-- 测试人员 -->
   <table id="testerTable" class="table table-bordered" style="width:700px;margin: 20px auto;">
   <thead style="background-color:#dce9eb"><tr ><th id="testerThead"></th>
   </tr></thead>
   
    </table>
   <!-- 表格 -->
   
   <table id="devDayFixTable" class="table table-bordered" style="width:700px; margin: 20px auto; ">
   <thead style="background-color:#dce9eb"><tr id="devDayFixThead"></tr></thead>
   <tbody style="text-align:center" id="devDayFixTbody"></tbody>
    </table>
   </div>
   
   <div style="margin-left:100px;display:none" id="nullCountTrendData" >
	<h3 style="margin:150px 0 150px 150px">暂无此项目报表数据</h3>
	</div>
   <div id="countTrendByDay">
   <!-- ehcarts -->
    <div id="countTrendEchart" style="margin:50px auto;width: 700px; height:400px;"></div>
   
   <table id="countTrendTable" class="table table-bordered" style="width:700px; margin: 20px auto; ">
   <thead style="background-color:#dce9eb">
    <th style="text-align:center">日期</th>
    <th style="text-align:center">日编写用例数</th>
   </thead>
   <tbody style="text-align:center" id="countTrendTbody"></tbody>
    </table>
   </div>
</div>

<script type="text/javascript" src="<%=request.getContextPath()%>/itest/js/analysisManage/writeCaseDayTrend.js"></script>
