<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<style>
 th{
  font-size: 13px;
    text-align: center;
 }
 
 tr td{
   text-align:center;
 }
</style>

<div> 
     <div style="padding: 30px 0 0 30px;">
    <label>版本：</label>
	 <select class="exui-combobox" id="bugDensityVer"  style="width:170px"/></select>	 
	</div>  
	<div style="display:none" id="nullBugDensityData" >
	  <h3 style="margin:150px 0 150px 150px">暂无此项目报表数据</h3>
	</div>
<div id="bugDensityContext">
   <!-- ehcarts -->
    <div id="bugDensityStat01Echarts" style="margin:50px auto;width: 700px; height:400px;"></div>
   <!-- ehcarts -->
    <div id="bugDensityStat02Echarts" style="margin:50px auto;width: 700px; height:400px;"></div>
   <!-- 表格 -->
   <table id="bugDensityStatTable" class="table table-bordered" style="width:700px;margin:50px auto">
   <thead style="text-align:center;background-color:#dce9eb">
     <tr>
       <th>一级测试需求</th>
       <th>BUG数</th>
       <th>Kloc</th>
       <th>密度</th>
       <th>所占百分比</th> 
     </tr>
   </thead>
   <tbody id="bugDensityStatTbody"></tbody>
    </table>
    <!-- ehcarts -->
    <div id="bugDensityStat03Echarts" style="margin:50px auto;width: 700px; height:400px;"></div>
    <!-- 表格 -->
   <table id="bugDensityStatTable01" class="table table-bordered" style="width:700px;margin:50px auto">
   <thead style="text-align:center;background-color:#dce9eb" id="bugDensityStatThead01" >
   </thead>
   <tbody id="bugDensityStatTbody01"></tbody>
    </table>
    
   <!-- ehcarts -->
    <div id="bugDensityStat04Echarts" style="margin:50px auto;width: 700px; height:400px;"></div>
    <!-- 表格 -->
   <table id="bugDensityStatTable02" class="table table-bordered" style="width:700px;margin:50px auto">
   <thead id="bugDensityStatThead02" style="text-align:center;background-color:#dce9eb">
   </thead>
   <tbody id="bugDensityStatTbody02"></tbody>
    </table>
    
   </div>
</div>

<script type="text/javascript" src="<%=request.getContextPath()%>/itest/js/analysisManage/bugDensityStat.js"></script>
