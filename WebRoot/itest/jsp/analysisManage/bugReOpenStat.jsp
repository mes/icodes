<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<style>
 .tools button,.tools button:hover{
  border: 1px solid #1E7CFB;
    color: #1E7CFB;
}

.tools button:first-of-type{
  background: #1E7CFB;
    border: 1px solid #1E7CFB;
    color: #ffffff;
}

.datebox, .datebox .textbox-text {
  width: 140px!important;
}

#proVersion_reOpen~.textbox {
   width: 130px!important;
}
</style>
<div>
   
   
<div class="tools" style="width:790px;margin:20px auto">  
	<span>开始：</span>
	<div class="input-field" style="width:150px;height:29px"> 
		<input class="exui-datebox" id="startDate_reOpen" editable="false" placeholder="请输入登陆账号"/>
	</div>
	<span>结束：</span>
	    
	<div class="input-field" style="width:150px;height:29px"> 
		<input class="exui-datebox" id="endDate_reOpen" editable="false" placeholder="请输入登陆账号"/>
	</div>
 <span>版本：</span>
 <div class="input-field" style="width:140px;height:29px"> 
   <input id="proVersion_reOpen" class="exui-combobox" editable="false" data-options="
            required:false,
            validateOnCreate:false, 
            prompt:'-请选择版本-'
     "/>
  </div>
 
	<button id="viewReOpenReport" type="button" style="margin-top:7px" class="btn btn-default" ><i class="glyphicon glyphicon-search"></i>查看报表</button>
	<button id="resetReOpenInp" type="button" style="margin-top:7px" class="btn btn-default" ><i class="glyphicon glyphicon-pencil"></i>重置</button>

</div>

<div id="mainContent">
   
   <!-- 表格 -->
   <table class="table table-bordered" style="width:700px;margin:50px auto">
   <caption>开发人员重开BUG明细</caption>
   <thead style="text-align:center;background-color:#dce9eb">
   <tr>
     <th style="text-align:center;">BUG_ID</th>
     <th style="text-align:center;">开发人员</th>
     <th style="text-align:center;">当前BUG状态</th>
     <th style="text-align:center;">重开次数</th>
   </tr>
   </thead>
   <tbody style="text-align:center" id="bugReOpenTbody"></tbody>
    </table>
   </div>
   </div>
</div>

<script type="text/javascript" src="<%=request.getContextPath()%>/itest/js/analysisManage/bugReOpenStat.js"></script>
