<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<style>
#chargeOwner-Wrapper th{
  font-size: 13px;
    text-align: center;
 }
 
#chargeOwner-Wrapper tr td{
   text-align:center;
 }
 
#chargeOwner-Wrapper  div.tips{
   width:700px;
   margin:22px auto;
 }
 
#chargeOwner-Wrapper p{
   font-size:15px;
   color:blue;
   font-weight:bold
 }
 
  .tools button,.tools button:hover{
  border: 1px solid #1E7CFB;
    color: #1E7CFB;
}

.tools button:first-of-type{
  background: #1E7CFB;
    border: 1px solid #1E7CFB;
    color: #ffffff;
}
.datebox, .datebox .textbox-text {
  width: 140px!important;
}
#proVersion_owner~.textbox {
   width: 140px!important;
}
#age_select~.textbox {
   width: 140px!important;
}
.chargeTable{
width:700px;
margin: 20px auto
}
.chargeTable>thead{
text-align:center;
background-color:#dce9eb
}
caption{
   font-size:15px;
   color:blue;
   font-weight:bold
 }
 .tools{
 width:90%;
 margin:20px auto
 }
</style>

<div id="chargeOwner-Wrapper">  
  <div class="tools" >  
		<span>开始：</span>
		<div class="input-field" style="width:150px;height:29px"> 
		<input class="exui-datebox" id="startDate" editable="false" placeholder="请输入登陆账号"/>
		</div>
		<span>结束：</span>
	    
		<div class="input-field" style="width:150px;height:29px"> 
			<input class="exui-datebox" id="endDate" editable="false" placeholder="请输入登陆账号"/>
		</div>
</div> 
<div class="tools" >
	  <span>版本：</span>
	 <div class="input-field" style="width:150px;height:29px"> 
	   <input id="proVersion_owner" class="exui-combobox" editable="false" data-options="
	            required:false,
	            validateOnCreate:false, 
	            prompt:'-请选择版本-'
	     "/>
	  </div>
	  <span>龄期：</span>
	  <div class="input-field" style="width:150px;height:29px"> 
	   <input id="age_select" class="exui-combobox" editable="false" data-options="
	            required:false,
	            validateOnCreate:false,  
	            valueField: 'label',
		        textField: 'value',
	            data: [{
			label: '1',
			value: '天'
		},{
			label: '7',
			value: '周'
		} ]"/>
	  </div>
		<button type="button" style="margin-top:7px" class="btn btn-default" id="viewReport"><i class="glyphicon glyphicon-search"></i>查看报表</button>
		<button type="button" style="margin-top:7px" class="btn btn-default" id="resetInp"><i class="glyphicon glyphicon-pencil"></i>重置</button>
	
</div>
  <div id="chargeOwnerDiv" style="display:none">
   <!-- ehcarts -->
    <div id="chargeOwnerEcharts" style="margin:50px auto 20px;width: 700px; height:400px;"></div>
   <!-- 表格 -->
   
   <table id="chargeOwnerTable" class="table table-bordered  chargeTable"  >
   <caption class="tableTitle" align="top">开发人员名下BUG数统计</caption>
   <thead >
     <tr>
       <th>人员姓名</th>
       <th>BUG数</th>
     </tr>
   </thead>
   <tbody id="chargeOwnerTbody"></tbody>
    </table>
    <!--bug状态按人分布 -->
    <table id="stateTable" class="table table-bordered chargeTable"   >
    <caption class="tableTitle" align="top">开发人员名下Bug按Bug状态分布统计</caption>
   <thead >
     <tr> 
	      <td style='text-align:center;'>开发人员</td>
	      <td style='text-align:center;'> BUG状态</td>
	      <td style='text-align:center;'>BUG数</td>
	    </tr>
   </thead>
   <tbody id="stateTableTbody"></tbody>
    </table>
    <!-- bug等级按人分布 -->
      <table id="levelTable" class="table table-bordered chargeTable"  >
      <caption class="tableTitle" align="top">开发人员名下Bug按Bug等级分布统计</caption>
   <thead >
     <tr>
        <td style='text-align:center;'>开发人员</td>
	      <td style='text-align:center;'> BUG等级</td>
	      <td style='text-align:center;'>BUG数</td>
     </tr>
   </thead>
   <tbody id="levelTableTbody"></tbody>
    </table>
    <!-- bug类型按人分布 -->
        <table id="typeTable" class="table table-bordered chargeTable"  >
        <caption class="tableTitle" align="top">开发人员名下Bug按Bug类型分布统计</caption>
   <thead >
     <tr>
       <td style='text-align:center;'>开发人员</td>
	      <td style='text-align:center;'> BUG类型</td>
	      <td style='text-align:center;'>BUG数</td>
     </tr>
   </thead>
   <tbody id="typeTableTbody"></tbody>
    </table>
    <!-- bug龄期按人分布 -->
        <table id="ageTable" class="table table-bordered chargeTable"  >
        <caption class="tableTitle" align="top">开发人员名下Bug按Bug龄期分布统计</caption>
   <thead >
      <tr>
       <td style='text-align:center;'>开发人员</td>
       <td style='text-align:center;'> 龄期</td>
	      <td style='text-align:center;'> BUG状态</td>
	      <td style='text-align:center;'>BUG数</td>
     </tr>
   </thead>
   <tbody id="ageTableTbody"></tbody>
    </table>
   </div>
   <div id="chargeOwnerMask" style="display:none">
     <div style='font-size: 25px;text-align: center;padding-top: 105px;'>暂无此项目报表数据</div>
   </div>
   <!-- <div class="tips">
     <p >下述状态的BUG不计为有效BUG:撤销、无效、重复、非错、关闭/撤销，修正/描述不当</p>
   </div> -->
</div>

<script type="text/javascript" src="<%=request.getContextPath()%>/itest/js/analysisManage/chargeOwner.js"></script>
