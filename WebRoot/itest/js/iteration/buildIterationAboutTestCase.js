var addOrEditPkgFlag = '';
var isCollapseFlag = false;
var iterationId = "";
//获取页面url参数
function getQueryParam(name) {
       var obj = $('#addOrEditCasePackageDlg').dialog('options');
       var queryParams = obj["queryParams"]; 
       return queryParams[name];
}

$(function(){
	$.parser.parse();
	addOrEditPkgFlag = getQueryParam('addOrEditPkgFlag');
	iterationId = getQueryParam('iterationId');
	loadExecutor();
    loadExecVersion();
    $("#testCaseModuleNums").textbox('setValue','');
    $("#moduleNames").html('');
	if(addOrEditPkgFlag == 'edit'){
		$("#exportOtherPkgs").css("display","none");
        $("#tipsline").css("display","none");
		$('#exportOtherModule').css("display","none");

		//修改用例包--获取用例包数据
		loadTestCasePkg();
	}else{
		$("#exportOtherPkgs").css("display","table-row");
		$("#tipsline").css("display","table-row");
		$('#exportOtherModule').css("display","table-row");
		loadOtherTestPkg();
	}
});

function loadExecutor(){
	$.post(
			baseUrl + "/otherMission/otherMissionAction!getPeopleLists.action",
			null,
			function(dat) {
				if (dat != null) {
					var peopleList = dat.rows;
					$("#executor").xcombobox({
						data:dat.rows,
						multiple:true,
						valueField:'id',
						textField:'name'
					});
				}
			}, "json")
}


function loadOtherTestPkg() {
	//var currTaksId = $("#taksIdmain").val();
	var currTaksId =$("#taskIdCl").val();
	$("#testPkgs").xcombobox({
		url:  baseUrl + '/testCasePkgManager/testCasePackageAction!loadTestCasePkgIds.action?dto.testCasePackage.taskId=' + currTaksId ,
		valueField: 'keyObj',
		textField: 'valueObj',
		multiple:true,
		onSelect: function() {
			//清空从测试用例分解树中导入的用例
			 $("#testCaseModuleNums").textbox('setValue','');
			 $("#moduleNames").html("");
		}
	});
}

function loadExecVersion() {
	//var currTaksId = $("#taksIdmain").val();
	var currTaksId =$("#taskIdCl").val();
	$("#execVersion").xcombobox({
		url: baseUrl + '/testTaskManager/testTaskManagerAction!loadVerSel.action?dto.taskId=' + currTaksId,
		valueField: 'keyObj',
		textField: 'valueObj',
		onSelect: function(rec) {
		}
	});
}

function loadTestCasePkg(){
	var row = casePkgObjs.$testCasePkgTb.xdatagrid('getSelected');
	var fillData = {};
	fillData["dto.testCasePackage"] = row;
//	casePkgObjs.isAddTestCasePkg = false;
	$.getJSON(
			baseUrl + '/testCasePkgManager/testCasePackageAction!getUserIdsByPackageId.action',
		{'dto.testCasePackage.packageId': row.packageId},
		function(data) {
			$('#addOrEditPkgWin').xform('clear');
			if(null!=data){
				$("#executor").xcombobox("setValue",data);
			}
			if(fillData["dto.testCasePackage.execVersion"]){				
				$("#execVersion").xcombobox("setValues",fillData["dto.testCasePackage.execVersion"]);
			}
			//赋值
//			setForm(row);
		$('#addOrEditPkgWin').xdeserialize(fillData);
		}
	);
}

//提交新增用例包
document.getElementById("submitPkgBtn").addEventListener('click',function(){
	var packageId = $("#packageId").val();
	var urlStr = "";
	var tipStr = "";
	var otherPkgIs = "";
	var packageName = $("#packageName").textbox('getValue');
	var executor =  $("#executor").xcombobox("getValues").toString();
	var expectedStartTime = $("#expectedStartTime").xdatebox('getValue');
	var expectedEndTime = $("#expectedEndTime").xdatebox('getValue');
	var actualStartTime = $("#actualStartTime").xdatebox('getValue');
   
	if(null == packageName || "" == packageName){
		$.xalert("请输入测试包名称");
		return ;
	}
	if(null == executor || "" == executor){
		$.xalert("请选择分配人");
		return ;
	}
	
	if(null == expectedStartTime || "" == expectedStartTime){
		$.xalert("请选择预计开始时间");
		return ;
	}
	
	if(null == expectedEndTime || "" == expectedEndTime){
		$.xalert("请选择预计结束时间");
		return ;
	}
	
	if ($("#execVersion").xcombobox("getValue") == "") {
		$.xalert({
			title: '提示',
			msg: '请选择执行版本！'
		});
		return;
	}
	
	if(null == packageId || "" == packageId){
	   urlStr = baseUrl + '/testCasePkgManager/testCasePackageAction!saveTestCasePackage.action';
	   tipStr = "新增成功";
	   otherPkgIds = $("#testPkgs").xcombobox("getValues").toString();
	   var otherPkgIdArr = otherPkgIds.split(',')
	   if(otherPkgIdArr.length>3){
			$.xalert({
				title: '提示',
				msg: '导入其它测试包用例不能超过3个！'
			});
			return;
	   }
	}else{
	   urlStr = baseUrl + '/testCasePkgManager/testCasePackageAction!updateTestCasePackage.action';
	   tipStr = "修改成功";
	}
	
	var data = $('#addOrEditPkgWin').xserialize();
	data["dto.selectedUserIds"] = $("#executor").xcombobox("getValues").toString();
	data["dto.testCasePackage.executor"] = $("#executor").xcombobox("getText");
	data["dto.testCasePackage.execVersion"] = $("#execVersion").xcombobox("getValues").toString();
	data["dto.iterationId"] = iterationId;
	data["dto.tagName"] = $("#tagName").xtextbox("getValue").trim();
	if(null == packageId || "" == packageId){		
		//只有新增测试包时可选择导入其它测试包的用例
		data["dto.otherTestPkgIds"] = otherPkgIds;
	}
	
	$.post(
			urlStr,
			data,
			function(dataObj) {
				if(dataObj.indexOf('reName') >= 0){
					$.xalert("测试包名称'" + packageName + "'已存在,请更换");
				}else{
					 $.xalert(tipStr);
				    $('#executor').combobox('clear');
				    $('#execVersion').combobox('clear');
					$('#addOrEditPkgWin').xform('clear');
					$('#addOrEditCasePackageDlg').xwindow('close');
		    	    casePkgObjs.$testCasePkgTb.xdatagrid('reload');
				}
			},"text"
		);
});


document.getElementById("closePkgWinBtn").addEventListener('click',function(){
	 $('#addOrEditCasePackageDlg').xwindow('close');
});

document.getElementById('showTestCaseTreeBtn').addEventListener('click',function(){
//	  var itemId = $("#taksIdmain").val();
		var itemId = $("#taskIdCl").val();
		var urlStr = baseUrl+"/caseManager/caseManagerAction!loadTree.action?dto.taskId="+itemId;
		$('#testCaseDetailTree').xtree({
		    url:urlStr,
		    method:'get',
			animate:true,
			lines:true,
			checkbox:true,
			onlyLeafCheck: true
			
		});
	$("#testCaseTreeWin").xwindow('setTitle','测试用例分解树').xwindow('open');
	$('#testCaseDetailTree').tree('expandAll');
})



function handleConfirmModule() {
	var nodes = $('#testCaseDetailTree').tree('getChecked');
	var moduleIds = "";
	var moduleNames = "";

	if( nodes.length > 3){
		$.xalert({title:'消息提示',msg:'您要导入的测试用例模块最多只能选择3个！'});
		return;
	}else if( nodes.length > 0){
		for(var i=0; i<nodes.length; i++){
			moduleIds += nodes[i].id + ",";
			moduleNames += nodes[i].text + ",";
		}
		moduleIds = moduleIds.substr(0,moduleIds.length-1);
		moduleNames = moduleNames.substr(0,moduleNames.length-1);
		//查询选择的测试模块用例是否超过200
		isOverlimitNum(moduleIds,moduleNames); 
		return;
	}else{
		$.xalert({title:'消息提示',msg:'请选择您要导入用例的模块！'});
		return;
	}
	
}

//查询选择的测试模块用例是否超过200
function isOverlimitNum(moduleIds,moduleNames) {
	//var currTaksId = $("#taksIdmain").val();
	var currTaksId =$("#taskIdCl").val()
	var urlStr = baseUrl + '/testCasePkgManager/testCasePackageAction!findCaseIdByModuleIds.action';
	var data = {}
	data["dto.otherCaseModuleIds"] = moduleIds
	$.post(
			urlStr,
			data,
			function(dataObj) {
				if(dataObj !== "failed"){
					dataObj=JSON.parse(dataObj)
					if(dataObj.otherCaseIdNum>200) {
						$.xalert({title:'消息提示',msg:'您要导入模块的测试用例超过200,请重新选择！'});
					}else {
						$("#testCaseModuleNums").textbox('setValue',dataObj.otherCaseModuleNum);
					    $("#moduleNames").html(moduleNames);
					    //清空从测试用例列表中导入的用例
					    $("#testPkgs").xcombobox("setValues","")
					    $("#testCaseTreeWin").xwindow('close');
					}
					 
				}else{
					$.xalert({title:'消息提示',msg:'系统错误,请稍后再试！'});
				}
			},"text"
		);
}

//展开/折叠树状图
function expandOrCollapseAll() {
	if(!isCollapseFlag){
		$('#testCaseDetailTree').tree('collapseAll');
	}else{
		$('#testCaseDetailTree').tree('expandAll');
	}
	isCollapseFlag = !isCollapseFlag;
}

//# sourceURL=buildIterationAboutTestCase.js