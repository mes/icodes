var reptfixItemId = "",
	reptfixProjectName = "",
	reptfixStartDate = "",
	reptfixEndDate = "",
	reptfixProNum = "";
//当前版本号
var currentVersion = "";

function getParamString(name) {
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
	var r = window.location.search.substr(1).match(reg);
	if (r != null) return unescape(r[2]);
	return null;
}

$(function() {
	$.parser.parse();
	reptfixItemId = $("#analyitemId").val();
	reptfixStartDate = $("#analyplanStartDate").val();
	reptfixEndDate = $("#analyplanEndDate").val();
	//加载版本下拉
	//loadDiscoverVersion();
	//加载ehcarts
	//getData(reptfixItemId,reptfixStartDate,reptfixEndDate);
	loadDiscoverVersion(reptfixItemId)
	getBugDate(reptfixItemId);
});

//加载版本号
function loadDiscoverVersion(currTaksId) {
	$.post(
			baseUrl + '/testTaskManager/testTaskManagerAction!loadVerSel.action?dto.taskId=' + currTaksId,
			{},
			function(data){
				data.unshift({keyObj:'',valueObj:'所有'});
				$("#proVersion_obscure").xcombobox({
                    data: data,
					valueField: 'keyObj',
					textField: 'valueObj',
				});
				
			},'json');
}

//获取有数据的时间
function getBugDate(id){
	var url = baseUrl + "/singleTestTask/singleTestTaskAction!getBugDateLimit.action";
	$.post(
			url,
			{	"dto.singleTest.taskId":id
				},
			function(data){
				if(data.length>0){
					var dateArray = data.split("_");
					$("#startDate_obscure").datebox("setValue",dateArray[0]); 
					$("#endDate_obscure").datebox("setValue",dateArray[1]);
					//加载BUG重开次数统计
					getBugobscureData(id,dateArray[0],dateArray[1],'');
				}
				},'text');
}

function getBugobscureData(itemId, startDate, endDate,versionCode) {
	//currentVersion = 
	var url = baseUrl + "/analysis/analysisAction!getObscureBugStat.action";
	$.post(
		url, {
			"analysisDto.taskId": itemId,
			"analysisDto.startDate": startDate,
			"analysisDto.endDate": endDate,
			"analysisDto.versionCode": versionCode
		},
		function(data) {
			if (data.length > 0) {
				displayTableData(data);
			} else {
				$('#bugobscureTbody').html('<tr><td>0</td><td>0</td><td>0</td><td>0</td></tr>') 
			}

		}, 'json');
}

function displayTableData(tableData) {
	var htmlStr = "";
	for(var i =0 ; i<tableData.length; i++) {
		htmlStr += '<tr><td>' + tableData[i].BUGCARDID + '</td>' + 
		       '<td>' + tableData[i].name + '</td>' + 
		       '<td>' + tableData[i].current_state + '</td>' + 
		       '<td>' + tableData[i].obscureCount + '</td>'       
	}
	$('#bugobscureTbody').html(htmlStr) 

}

//查看报表
document.getElementById("viewobscureReport").addEventListener('click', function() {
	var itemId = "",
		startDate = "",
		endDate = "",
		versionCode="";
	startDate = $("#startDate_obscure").datebox('getValue');
	endDate = $("#endDate_obscure").datebox('getValue');
	 versionCode = $('#proVersion_obscure').xcombobox('getValue');
	if (null == startDate || startDate == "") {
		$.xalert("请选择开始日期");
		return;
	}

	if (null == endDate || endDate == "") {
		$.xalert("请选择结束日期日期");
		return;
	}
	endDate = getNewDay(endDate, '1');
	getBugobscureData(reptfixItemId, startDate, endDate,versionCode);
	
});

//重置输入框
document.getElementById("resetobscureInp").addEventListener('click', function() {
	$('#startDate_obscure').datebox('setValue', '');
	$('#endDate_obscure').datebox('setValue', '');
	$("#proVersion_obscure").xcombobox('setValue','');

});

function getNewDay(dateTemp, days) {

	dateTemp = dateTemp.replace(new RegExp(/-/gm), "/");
	var nDate = new Date(dateTemp);
	var millSeconds = Math.abs(nDate) + (days * 24 * 60 * 60 * 1000);
	var rDate = new Date(millSeconds);
	var year = rDate.getFullYear();
	var month = rDate.getMonth() + 1;
	if (month < 10) month = "0" + month;
	var date = rDate.getDate();
	if (date < 10) date = "0" + date;
	return ("" + year + "" + "-" + "" + month + "" + "-" + "" + date + "");
}


//@ sourceURL=bugReOpenStat.js