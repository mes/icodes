var legendData = [];
var functionName = [];
var bugCount = [];
var bugDensity = [];
var klocNum = [];
var bugType = [];
var seriesData = []; 
var bugPercent = [];

var bugDensityItemId = null;
var bugDensityStat01Echart = null;
var bugDensityStat02Echart = null;
var bugDensityStat03Echart = null;
var bugDensityStat04Echart = null;

var isValueFlag = false; //是否有数据。有：绘制图表  无：页面显示该项目没有数据
var countPregressNum = 0; //ajax异步执行，此变量用于计数
$(function(){
	$.parser.parse();
	//待处理BUG按天龄期分析
	bugDensityItemId = $("#analyitemId").val(); 
	loadDiscoverVersion();

});



//加载版本号
function loadDiscoverVersion() {
	$.post(
			baseUrl + '/testTaskManager/testTaskManagerAction!loadVerSel.action?dto.taskId=' + bugDensityItemId,
			{},
			function(data){
				data.unshift({keyObj:'',valueObj:'所有'});
				$("#bugDensityVer").xcombobox({
                  data: data,
					valueField: 'keyObj',
					textField: 'valueObj',
					onSelect: function(res) {
						 legendData = [];
						 functionName = [];
						 bugCount = [];
						 bugDensity = [];
						 klocNum = [];
						 bugType = [];
						 seriesData = []; 
						 bugPercent = [];
						 countPregressNum=0;
						getBugDensityStat(res.keyObj);
						 
						getBugDensityStatType(res.keyObj);
						
						getBugDensityStatBugType(res.keyObj);
					}
				});
				
			},'json');
}

//待处理BUG按天龄期分析
function getBugDensityStat(currentVersion){
	var url = baseUrl + "/analysis/analysisAction!getBugDensityStat.action";
	$.post(
			url,
			{	"analysisDto.taskId":bugDensityItemId,
				"analysisDto.versionCode": currentVersion
				},
			function(data){
			  countPregressNum++;
			  if(data.length>0){
				  isValueFlag = true;
				  $("#nullBugDensityData").css('display', 'none');
				   $("#bugDensityContext").css('display', 'block');
				  $("#bugDensityStat01Echarts").css('display', 'block');
				   $("#bugDensityStat02Echarts").css('display', 'block');
				   $("#bugDensityStatTable").css('display', 'table');
				   //构造echarts数据
				   constrEchartsData(data,'1');
				   loadEcharts('一级测试需求bug分布','bugDensityStat01Echarts');
				   constrEchartsData(data,'2');
				   loadEcharts('一级测试需求bug密度','bugDensityStat02Echarts');
				   loadTable(data,'bugDensityStatThead','bugDensityStatTbody','0'); 
			   }else{
				   if(countPregressNum === 3 && !isValueFlag){
					   $("#nullBugDensityData").css('display', 'block');
					   $("#bugDensityContext").css('display', 'none');
				   }else{
					   $("#bugDensityStat01Echarts").css('display', 'none');
					   $("#bugDensityStat02Echarts").css('display', 'none');
					   $("#bugDensityStatTable").css('display', 'none');
				   }
			   }
			},'json');
}

//待处理BUG按天绝对龄期分析
function getBugDensityStatType(currentVersion){
	var url = baseUrl + "/analysis/analysisAction!getBugDensityStatType.action";
	$.post(
			url,
			{	"analysisDto.taskId":bugDensityItemId,
				"analysisDto.versionCode": currentVersion
				},
			function(data){
			countPregressNum++;
			 if(data.length>0){
				 isValueFlag = true;
				  $("#nullBugDensityData").css('display', 'none');
				  $("#bugDensityContext").css('display', 'block');
				  $("#bugDensityStat03Echarts").css('display', 'block');
				  $("#bugDensityStatTable01").css('display', 'table');
				   //构造echarts数据
				   constrEchartsData(data,'3');
				   loadEcharts('一级测试需求bug等级分布','bugDensityStat03Echarts'); 
				   loadTable(data,'bugDensityStatThead01','bugDensityStatTbody01','1'); 
			   }else {
				   if(countPregressNum === 3 && !isValueFlag){
					   $("#nullBugDensityData").css('display', 'block');
					   $("#bugDensityContext").css('display', 'none');
				   }else{
					   $("#bugDensityStat03Echarts").css('display', 'none');
					   $("#bugDensityStatTable01").css('display', 'none');
				   }
			   }
			},'json');
}
function getBugDensityStatBugType(currentVersion){
	var url = baseUrl + "/analysis/analysisAction!getBugDensityStatBugType.action";
	$.post(
			url,
			{	"analysisDto.taskId":bugDensityItemId,
				"analysisDto.versionCode": currentVersion
				},
			function(data){
			 countPregressNum++;
			 if(data.length>0){
				 isValueFlag = true;
				   $("#nullBugDensityData").css('display', 'none');
				   $("#bugDensityContext").css('display', 'block');
				   $("#bugDensityStat04Echarts").css('display', 'block');
				   $("#bugDensityStatTable02").css('display', 'table');
				   //构造echarts数据
				   constrEchartsData(data,'3');
				   loadEcharts('一级测试需求bug类型分布','bugDensityStat04Echarts'); 
				   loadTable(data,'bugDensityStatThead02','bugDensityStatTbody02','1'); 
			   }else{
				   if(countPregressNum === 3 && !isValueFlag){
					   $("#nullBugDensityData").css('display', 'block');
					   $("#bugDensityContext").css('display', 'none');
				   }else{
					   $("#bugDensityStat04Echarts").css('display', 'none');
					   $("#bugDensityStatTable02").css('display', 'none');
				   } 
			   }
			},'json');
}
//构造echarts图表数据--两种数据
function constrEchartsData(object,flag){
    getNameAndDate(object.concat(),flag);
	getSeriesData(object.concat(),flag);
}


//获取echarts的类别、日期

function getNameAndDate(sourceData,flag){
	functionName = [];
	bugCount = [];
	bugDensity = [];
	klocNum = [];
	bugType = [];
	var bugDen = 0;
	if(flag=='3'){
		for(var i=0;i<sourceData.length;i++){
			if(bugType.indexOf(sourceData[i][1]) == -1 ){
				bugType.push(sourceData[i][1]);
			}
			if(functionName.indexOf(sourceData[i][0]) == -1 ){
				functionName.push(sourceData[i][0]);
			}
		}
		legendData = bugType;
	}else{
		legendData = ['bug数']
		for(var i=0;i<sourceData.length;i++){
			functionName.push(sourceData[i][0]);
			bugCount.push(sourceData[i][2]);
			klocNum.push(sourceData[i][1])
			if(sourceData[i][1]=='0'){
			   bugDen = 0;
			}else{
	           bugDen = (sourceData[i][2]/sourceData[i][1]).toFixed(2);	
			}
			bugDensity.push(bugDen);
		}
	}
 
}

//获取echarts的seriesData数据

function getSeriesData(data,flag){
	seriesData =[]; 
	if(flag=='1'){
		var kkk = {
		name: 'bug个数',
		type: 'bar', 
		};
		var lkl = [];
	for(var i=0;i<functionName.length;i++){
		for(var j=0;j<data.length;j++){
			if(data[j][0]==functionName[i]){
				lkl.push(data[j][2])
			}
		}
	}
	kkk["data"] = lkl;
	seriesData.push(kkk);
	}else if(flag=='2'){
		var kkk = {
				name: 'bug密度',
				type: 'bar', 
				
				}; 
			 
			kkk["data"] = bugDensity;
			seriesData.push(kkk);
	}else if(flag=='3'){
		for(var a=0;a<bugType.length;a++){
			var kkk = {
					name: bugType[a],
					type: 'bar', 
			};
			var lkl = [];
			for(var b=0;b<functionName.length;b++){
				var hjk = 1;
				for(var c=0;c<data.length;c++){
					if(bugType[a]==data[c][1]&&functionName[b]==data[c][0]){
						lkl.push(data[c][2]);
						hjk = 1;
						break;
					}else{
						hjk = hjk + 1;
					}
				}
				if(hjk != 1){
					lkl.push(0);
				}
			}
			kkk["data"] = lkl;
			kkk["stack"] = 'bug'
			
			seriesData.push(kkk);
		} 
	} 
}

function loadEcharts(title,echartsId){
	var myChart = null;
	if(echartsId === "bugDensityStat01Echarts"){
		 if(bugDensityStat01Echart){
			 bugDensityStat01Echart.clear()
		 }else{			 
			 bugDensityStat01Echart = echarts.init(document.getElementById(echartsId));
		 }
		 myChart = bugDensityStat01Echart;
	}else if(echartsId === "bugDensityStat02Echarts"){
		 if(bugDensityStat02Echart){
			 bugDensityStat02Echart.clear()
		 }else{			 
			 bugDensityStat02Echart = echarts.init(document.getElementById(echartsId));
		 }
		 myChart = bugDensityStat02Echart;

	}else if(echartsId === "bugDensityStat03Echarts"){
		 if(bugDensityStat03Echart){
			 bugDensityStat03Echart.clear()
		 }else{			 
			 bugDensityStat03Echart = echarts.init(document.getElementById(echartsId));
		 }
		 myChart = bugDensityStat03Echart;
	}else if(echartsId === "bugDensityStat04Echarts"){
		 if(bugDensityStat04Echart){
			 bugDensityStat04Echart.clear()
		 }else{			 
			 bugDensityStat04Echart = echarts.init(document.getElementById(echartsId));
		 }
		 myChart = bugDensityStat04Echart;
	}

    // 指定图表的配置项和数据
	var option = {
		    title: {
		        text: title
		    },
		    tooltip: {
		        trigger: 'axis'
		    },
		    legend: {
		    	x: 'right',
		        data: legendData,
		        orient: 'vertical',
		    	width: 250,
		    	top: 50, 
		    	left:600
		    },
		    grid: {
		        left: '3%',
		        right: '20%',
		        bottom: '3%',
		        containLabel: true
		    },
		    toolbox: {
				show: true,
				feature: {
					mark: {
						show: false
					},
					dataView: {
						show: false,
						readOnly: false
					}, 
					restore: {
						show: true
					},
					saveAsImage: {
						show: false
					}
				},
				right:'20%'
			},
		    xAxis: {
		        type: 'category', 
		        data: functionName,
                interval: 0,
                formatter:function(value)
                {
                    return value.split("").join("\n");
                }
		    },
		    yAxis: {
		        type: 'value'
		    },
		    series: seriesData
		};

    // 使用刚指定的配置项和数据显示图表。
    myChart.setOption(option);
}


//加载表格
function loadTable(data,theadId,tbodyId,flag){
	var tbody = "";
	if(flag=='0'){
		bugPercent = []; 
		  
		  var sum = 0;
		  for(var i=0;i<bugCount.length;i++){
			 sum += bugCount[i]
		  }
		  var bugPercentSingle = 0;
		  for(var i=0;i<bugCount.length;i++){
			  bugPercentSingle = ((bugCount[i]/sum)*100).toFixed(2)+"%";
			  bugPercent.push(bugPercentSingle)
		  }
		  for(var i=0;i<functionName.length;i++){
			  tbody += "<tr><td>"+functionName[i]+"</td>" +
			  		"<td>"+bugCount[i]+"</td>" +
			  				"<td>"+klocNum[i]+"</td>" +
			  						"<td>"+bugDensity[i]+"</td>" +
			  								"<td>"+bugPercent[i]+"</td></tr>";
		  } 
		   document.getElementById(tbodyId).innerHTML = tbody;
	}else{
		var thead = "<tr><td>一级测试需求</td>";
		for(i=0;i<bugType.length;i++){
			thead += "<td>"+bugType[i]+"</td>";
		}
		thead += "<td>合计</td></tr>"
		document.getElementById(theadId).innerHTML = thead;
		var tbodyData = [];
		for(var i=0;i<functionName.length;i++){
			var sum = 0;
            var tbodyDataSingle = [];
			tbodyDataSingle.push(functionName[i]);
			for(var j=0;j<bugType.length;j++){
				var flag01 = 1;
				for(var k=0;k<data.length;k++){
					if(data[k][0]==functionName[i]&&data[k][1]==bugType[j]){
						tbodyDataSingle.push(data[k][2])
						flag01 = 1;
						sum += data[k][2]
                        break 
					}else{
						flag01 += 1;
					}
					
				}
				if(flag01!=1){
					tbodyDataSingle.push('-');
				}
				
			}
			tbodyDataSingle.push(sum);
			tbodyData.push(tbodyDataSingle);
		}
	
		for(var i=0;i<tbodyData.length;i++){
			tbody +="<tr>"
			for(var j=0;j<tbodyData[i].length;j++){
				if(j>=tbodyData[i].length-1){
					 tbody += "<td style='background: #f0f1b3;'>"+tbodyData[i][j]+"</td>";
				}else{
					 tbody += "<td>"+tbodyData[i][j]+"</td>";
				}
               
			}
			tbody += "</tr>"
		}
		
		document.getElementById(tbodyId).innerHTML = tbody;  
	}
	
}
//@ sourceURL=bugDensityStat.js
