
$(function(){
	$.parser.parse();
	//加载ehcarts
	var itemId="";
	itemId = $("#analyitemId").val(); 
	//加载版本下拉
	$("#proVersion_summary").xcombobox({
		url: baseUrl + '/testTaskManager/testTaskManagerAction!loadVerSel.action?dto.taskId=' + itemId,
		loadFilter: function(data) {
			var obj = {};
			obj.keyObj = "";
			obj.valueObj = '所有'
			data.splice(0, 0, obj) //在数组0位置插入obj,不删除原来的元素
			return data;
		},
		onLoadSuccess: function(res) {
			var data = $("#proVersion_summary").xcombobox('getData'); //获取所有下拉框数据
			if (data.length > 0) {
				//如果有数据的话默认选中第一条数据
				$("#proVersion_summary").xcombobox('select', data[0].valueObj);
			}
		},

		valueField: 'keyObj',
		textField: 'valueObj',
		onSelect: function(res) {
			currentVersion = res.keyObj;
		}
	});
	//获取时间
	getBugDate(itemId);
});

function getBugDate(id){
	var url = baseUrl + "/singleTestTask/singleTestTaskAction!getBugDateLimit.action";
	$.post(
			url,
			{	"dto.singleTest.taskId":id
				},
			function(data){
				if(data.length>0){
					var dateArray = data.split("_");
					$("#startDate").datebox("setValue",dateArray[0]); 
					$("#endDate").datebox("setValue",dateArray[1]);
					loadNewFixCloseBugSummary(id,dateArray[0],dateArray[1]);
					loadBeforeOpenBugSummary(id,dateArray[0],dateArray[1]);
					//新增的四个统计
					loadTesterBugsTable(id,dateArray[0],dateArray[1]);
					loadBugLevelTable(id,dateArray[0],dateArray[1]);
					loadExeCaseTable(id,dateArray[0],dateArray[1]);
					loadWriteCaseTable(id,dateArray[0],dateArray[1]);
				}
				},'text');
}

//简要统计--时间段内新增、修改和关闭BUG概况
function loadNewFixCloseBugSummary(itemId,startDate,endDate){
	var url = baseUrl + "/analysis/analysisAction!getNewFixCloseBugSummary.action";
	var obj ={};
	if($("#includeChange01").is(':checked')){
		obj = {
				"analysisDto.taskId":itemId,
				"analysisDto.startDate":startDate,
				"analysisDto.endDate":endDate,
				"analysisDto.versionCode": currentVersion,
				"analysisDto.parameter":1,
		}
	}else{
		obj = {
				"analysisDto.taskId":itemId,
				"analysisDto.startDate":startDate,
				"analysisDto.endDate":endDate,
				"analysisDto.versionCode": currentVersion
		}
	}
	$.post(
			url,obj,
			function(data){
			   if(data.length>0){
				   loadNewFixCloseBugTable(data);
			   }
			},'json');
}

//简要统计--截止到时间末BUG状态分布情况
function loadBeforeOpenBugSummary(itemId,startDate,endDate){
	var url = baseUrl + "/analysis/analysisAction!getBeforeOpenBugSummary.action";
	var obj ={};
	if($("#includeChange01").is(':checked')){
		obj = {
				"analysisDto.taskId":itemId,
				"analysisDto.startDate":startDate,
				"analysisDto.endDate":endDate,
				"analysisDto.versionCode": currentVersion,
				"analysisDto.parameter":1,
		}
	}else{
		obj = {
				"analysisDto.taskId":itemId,
				"analysisDto.startDate":startDate,
				"analysisDto.endDate":endDate,
				"analysisDto.versionCode": currentVersion
		}
	}
	$.post(
			url,obj,
			function(data){
			   if(data.length>0){
				   loadBeforeOpenBugTable(data);
			   }
			},'json');
}

function loadNewFixCloseBugTable(data){
	 
	  var thead = "<tr>";
	  var tbody = "<tr>";
	  for(var i=0;i<data.length;i++){
		 switch(data[i][3]){
		    case 1:
		    	thead += "<th style='text-align:center;'>修改 </th>";
		    	tbody += "<td style='text-align:center;'>" + data[i][0] + "</td>";
		    	break;
		    case 2:
		    	thead += "<th style='text-align:center;'>关闭</th>";
		    	tbody += "<td style='text-align:center;'>" + data[i][1] + "</td>";
		    	break;
		    case 3:
		    	thead += "<th style='text-align:center;'>新增</th>";
		    	tbody += "<td style='text-align:center;'>" + data[i][2] + "</td>";
		    	break; 
		 }
	  } 
	  thead += "</tr>";
	  tbody += "</tr>";
	 $("#bugNewFixCloseSummaryThead").html(thead);
	 $("#bugNewFixCloseSummaryTbody").html(tbody);
}

function loadBeforeOpenBugTable(data){
	 document.getElementById("bugBeforeOpenSummaryTbody").innerHTML = "";
	  var tbody = "";
	  for(var i=0;i<data.length;i++){
		  tbody += "<tr><td style='text-align:center;'>"+ data[i][2] + "</td>" +
			       "<td style='text-align:center;'>" + data[i][0] + "</td>" + 
		           "<td style='text-align:center;'>" + data[i][1] + "</td></tr>" ;
	  } 
	 $("#bugBeforeOpenSummaryTbody").html(tbody);
}

//查看报表
document.getElementById("viewReport").addEventListener('click',function(){
	var itemId = "",startDate="",endDate="";
	itemId = $("#analyitemId").val(); 
	var startDate = $("#startDate").datebox('getValue'); 
	var endDate = $("#endDate").datebox('getValue'); 
	if(null == startDate || startDate == ""){
		$.xalert("请选择开始日期");
		return;
	}
	
	if(null == endDate || endDate == ""){
		$.xalert("请选择结束日期日期");
		return;
	}
	
	loadNewFixCloseBugSummary(itemId,startDate,endDate);
	loadBeforeOpenBugSummary(itemId,startDate,endDate);
	//新增的四个统计
	loadTesterBugsTable(itemId,startDate,endDate);
	loadBugLevelTable(itemId,startDate,endDate);
	loadExeCaseTable(itemId,startDate,endDate);
	loadWriteCaseTable(itemId,startDate,endDate);
});

//重置输入框
document.getElementById("resetInp").addEventListener('click',function(){
	$('#startDate').datebox('setValue', '');	
	$('#endDate').datebox('setValue', '');	
	$('#proVersion_summary').xcombobox('setValue', '');
});
//测试人员名下BUG汇总
function loadTesterBugsTable(itemId,startDate,endDate){
	var url = baseUrl + "/analysis/analysisAction!getTestBugSummary.action";
	var obj ={};
	if($("#includeChange01").is(':checked')){
		obj = {
				"analysisDto.taskId":itemId,
				"analysisDto.startDate":startDate,
				"analysisDto.endDate":endDate,
				"analysisDto.versionCode": currentVersion,
				"analysisDto.parameter":1,
		}
	}else{
		obj = {
				"analysisDto.taskId":itemId,
				"analysisDto.startDate":startDate,
				"analysisDto.endDate":endDate,
				"analysisDto.versionCode": currentVersion
		}
	}
	$.post(
			url,obj,
			function(data){
			  if(data.length>0){
				  $("#testerBugsTable").css('display','');
				  var html = "";
				  data.map(function(item){
					  var invalidPercent,bugNumPercent;
					  var obscurePercent;
					 if(item.bug_count==0){
						  invalidPercent = 0;
						  bugNumPercent = 0;
						  obscurePercent = 0;
					  }else{
						bugNumPercent = getShortLengthNum(item.bug_count/item.allof_bug_count)+"%";
					  }
					 if(item.invalid_bug_count==0){
						  invalidPercent = 0;
					  }else{
						  invalidPercent = getShortLengthNum(item.invalid_bug_count/item.bug_count)+"%";
					  } 
					 if(item.obscureCount==0){
						 obscurePercent = 0;
					 }else{
						 obscurePercent = getShortLengthNum(item.obscureCount/item.bug_count)+"%";
					 }
						  html += "<tr><td>"+item.name+"</td>" +
						  		"<td>"+item.bug_count+"</td>" +
						  				"<td>"+bugNumPercent+"</td>" +
						  						"<td>"+item.invalid_bug_count+"</td>" +
						  								"<td>"+invalidPercent+"</td>" +
						  										"<td>"+item.obscureCount+"</td>" +
						  										      "<td>"+obscurePercent+"</td></tr>";
				  })
				  $("#testerBugsTbody").html(html);
			  }else{
				  $("#testerBugsTable").css('display','none');
			  }
			},'json');
}
//测试人员BUG等级分布情况 
function loadBugLevelTable(itemId,startDate,endDate){
	var url = baseUrl + "/analysis/analysisAction!getTestBugLevelSummary.action";
	var obj ={};
	if($("#includeChange01").is(':checked')){
		obj = {
				"analysisDto.taskId":itemId,
				"analysisDto.startDate":startDate,
				"analysisDto.endDate":endDate,
				"analysisDto.versionCode": currentVersion,
				"analysisDto.parameter":1,
		}
	}else{
		obj = {
				"analysisDto.taskId":itemId,
				"analysisDto.startDate":startDate,
				"analysisDto.endDate":endDate,
				"analysisDto.versionCode": currentVersion
		}
	}
	$.post(
			url,obj,
			function(data){
			  if(data.length>0){
				  $("#bugLevelTable").css('display','');
				  var html = "";
				  data.map(function(item){
					  html += "<tr><td>"+item.NAME+"</td>" +
				  		"<td>"+item.bug_level+"</td>" +
				  				"<td>"+item.bugCount+"</td></tr>";
		 
				  })
				  $("#bugLevelTbody").html(html);
			  }else{
				  $("#bugLevelTable").css('display','none');
			  }
			},'json');
}
//测试人员执行用例情况 
function loadExeCaseTable(itemId,startDate,endDate){
var url = baseUrl + "/analysis/analysisAction!getTestExeCaseSummary.action";
var obj ={};
if($("#includeChange01").is(':checked')){
	obj = {
			"analysisDto.taskId":itemId,
			"analysisDto.startDate":startDate,
			"analysisDto.endDate":endDate,
			"analysisDto.versionCode": currentVersion,
			"analysisDto.parameter":1,
	}
}else{
	obj = {
			"analysisDto.taskId":itemId,
			"analysisDto.startDate":startDate,
			"analysisDto.endDate":endDate,
			"analysisDto.versionCode": currentVersion
	}
}
	$.post(
			url,obj,
			function(data){
			  if(data.length>0){
				  $("#exeCaseTable").css('display','');
				  var html = "";
				  data.map(function(item){
					  if(item.rest==2){
						  html += "<tr><td>"+item.name+"</td>" +
					  		"<td>"+item.exeCount+"</td>" +
					  		"<td>-</td><td>-</td>" +
					  				"<td>"+item.exeCost+"</td></tr>";
					  }else if(item.rest==3){
						  html += "<tr><td>"+item.name+"</td>" +
					  		"<td>-</td>" +
					  		"<td>"+item.exeCount+"</td><td>-</td>" +
					  				"<td>"+item.exeCost+"</td></tr>";
					  }else if(item.rest==5){
						  html += "<tr><td>"+item.name+"</td>" +
					  		"<td>-</td>" +
					  		"<td>-</td><td>"+item.exeCount+"</td>" +
					  				"<td>"+item.exeCost+"</td></tr>";
					  }
					  
		 
				  })
				  $("#exeCaseTbody").html(html);
			  }else{
				  $("#exeCaseTable").css('display','none');
			  }
			},'json');
}
//测试人员编写用例情况
function loadWriteCaseTable(itemId,startDate,endDate){
var url = baseUrl + "/analysis/analysisAction!getTestWriteCaseSummary.action";
var obj ={};
if($("#includeChange01").is(':checked')){
	obj = {
			"analysisDto.taskId":itemId,
			"analysisDto.startDate":startDate,
			"analysisDto.endDate":endDate,
			"analysisDto.versionCode": currentVersion,
			"analysisDto.parameter":1,
	}
}else{
	obj = {
			"analysisDto.taskId":itemId,
			"analysisDto.startDate":startDate,
			"analysisDto.endDate":endDate,
			"analysisDto.versionCode": currentVersion
	}
}
	$.post(
			url,obj,
			function(data){
			  if(data.length>0){
				  $("#writeCaseTable").css('display','');
				  var html = "";
				  data.map(function(item){
					  html += "<tr><td>"+item.name+"</td>" + 
				  				"<td>"+item.caseCount+"</td></tr>";
		 
				  })
				  $("#writeCaseTbody").html(html);
			  }else{
				  $("#writeCaseTable").css('display','none');
			  }
			},'json');
}
//保留4位小数
function getShortLengthNum(num){
	num = num*100;
	if((num.toString()).split(".").length > 1){
		var num01 =  (num.toString()).split(".")[0];//整数部分
		var num02 = (num.toString()).split(".")[1].substring(0,2);//保留2位小数
		var num03 = Number(num01 + "." +num02); 
		return num03;
	}else{
		return num.toString();
	}
}
//# sourceURL=bugSummary.js
