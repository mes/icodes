var seriesData = [];
var names = [];
//
var currentVersion = "";
var itemId = ""; 
//
var testerBugQualityEcharts =null;
$(function(){
	$.parser.parse();
	//加载ehcarts 
	itemId = $("#analyitemId").val(); 
	//
	$("#testerBugVersion").xcombobox({
		url: baseUrl + '/testTaskManager/testTaskManagerAction!loadVerSel.action?dto.taskId=' + itemId,
		loadFilter: function(data) {
			var obj = {};
			obj.keyObj = "";
			obj.valueObj = '所有'
			data.splice(0, 0, obj) //在数组0位置插入obj,不删除原来的元素
			return data;
		},
		onLoadSuccess: function(res) {
			var data = $("#testerBugVersion").xcombobox('getData'); //获取所有下拉框数据
			if (data.length > 0) {
				//如果有数据的话默认选中第一条数据
				$("#testerBugVersion").xcombobox('select', data[0].valueObj);
			}
		},

		valueField: 'keyObj',
		textField: 'valueObj',
		onSelect: function(res) {
			//console.log(res);
			currentVersion = res.keyObj;
			loadPie();
		}
	});
	//
	loadPie();

});
function loadPie(){
	seriesData = [];
	names = []; 
	var url = baseUrl + "/analysis/analysisAction!getTesterBugQuality.action";
	$.post(
			url,
			{"analysisDto.taskId":itemId,
			 "analysisDto.versionCode": currentVersion	
				},
			function(data){
			   if(data.length>0){
				   //构造echarts数据
				   constrEchartsData(data);
				   loadEcharts('测试人员BUG质量分析','testerBugQualityEcharts');
				   loadTable(data);
			   }
			},'json');
}
//构造echarts图表数据
function constrEchartsData(object){
	getType(object.concat());
	getSeriesData(object.concat());
}

//获取echarts的类别、日期

function getType(sourceData){
	for(var i=0;i<sourceData.length;i++){
		if(names.indexOf(sourceData[i][0]) == -1){
			names.push(sourceData[i][0]);
		}
	}
}

//获取echarts的seriesData数据

function getSeriesData(sourData){
	for(var i=0;i<names.length;i++){
		var obj = {
			      name:names[i]
		}
		for(var j=0;j<sourData.length;j++){
			if(sourData[j][0] == names[i]){
				obj.value = sourData[j][1];
				break;
			}
		}
		seriesData.push(obj);
	}
}

function loadEcharts(title,echartsId){
	var myChart = null;
	if(echartsId == "testerBugQualityEcharts"){
		if(testerBugQualityEcharts){
			testerBugQualityEcharts.clear();
		}else{
			testerBugQualityEcharts = echarts.init(document.getElementById(echartsId));
		}
	myChart = testerBugQualityEcharts;
	}
	

    // 指定图表的配置项和数据
       var	option = {
		    title : {
		        text: title,
		        x:'center'
		    },
		    tooltip : {
		        trigger: 'item',
		        formatter: "{a} <br/>{b} : {c} ({d}%)"
		    },
		    legend: {
		        type: 'scroll',
		        orient: 'vertical',
		        right: 10,
		        top: 20,
		        bottom: 20,
		        data: names
		    },
		    series : [
		        {
		            name: '测试人员',
		            type: 'pie',
		            radius : '55%',
		            center: ['40%', '50%'],
		            data: seriesData,
		            label: {
		                normal: {
		                    formatter:  ' {b} {c}  {per|{d}%}',
		                    backgroundColor: '#eee',
		                    borderColor: '#aaa',
		                    borderWidth: 1,
		                    borderRadius: 4,
		                    // shadowBlur:3,
		                    // shadowOffsetX: 2,
		                    // shadowOffsetY: 2,
		                    // shadowColor: '#999',
		                    // padding: [0, 7],
		                    rich: {
		                        a: {
		                            color: '#999',
		                            lineHeight: 22,
		                            align: 'center'
		                        },
		                        // abg: {
		                        //     backgroundColor: '#333',
		                        //     width: '100%',
		                        //     align: 'right',
		                        //     height: 22,
		                        //     borderRadius: [4, 4, 0, 0]
		                        // },
		                        hr: {
		                            borderColor: '#aaa',
		                            width: '100%',
		                            borderWidth: 0.5,
		                            height: 0
		                        },
		                        b: {
		                            fontSize: 16,
		                            lineHeight: 33
		                        },
		                        per: {
		                            color: '#eee',
		                            backgroundColor: '#334455',
		                            padding: [2, 4],
		                            borderRadius: 2
		                        }
		                    }
		                }
		            },
		            itemStyle: {
		                emphasis: {
		                    shadowBlur: 10,
		                    shadowOffsetX: 0,
		                    shadowColor: 'rgba(0, 0, 0, 0.5)'
		                }
		            }
		        }
		    ]
		};
    // 使用刚指定的配置项和数据显示图表。
    myChart.setOption(option);
}

//加载表格
function loadTable(data){
	  var tbody = "";
	  var columnOne=0,columnTwo=0,total3=0,total4=0,total5 = 0; 
	  var bugPercent = 0,bugPercentNum=0;
	  var obscurePercent = 0,obscurePercentNum=0; 
	  for(var i=0;i<seriesData.length;i++){
		  for(var j=0;j<data.length;j++){
			  if(seriesData[i].name == data[j][0]){
				  if(data[j][1]==0){
					  bugPercent = 0;
					  bugPercentNum =0;
					  obscurePercent = 0; 
					  obscurePercentNum=0;
				  }else{
					  bugPercentNum = getShortLengthNum(data[j][1]/data[j][4]);
					  bugPercent =  bugPercentNum+"%";
				  }
				  if(data[j][3]==0){
					  obscurePercent = 0; 
					  obscurePercentNum=0;
				  }else{
					  obscurePercentNum= getShortLengthNum(data[j][3]/data[j][1]);
					  obscurePercent =  obscurePercentNum+"%";
				  }
				  
				  tbody += "<tr><td>"+ data[j][0] + "</td>" +
		                    "<td>" + data[j][1] + "</td>" + 
		                    "<td>" + bugPercent + "</td>" + 
		                    "<td>" + data[j][2] + "</td>" + 
		                    "<td> 0.00</td>" + 
		                    "<td>" + data[j][3] + "</td>" + 
		                    "<td>" + obscurePercent+ "</td>" + 
		                    "</tr>";
				  columnOne +=data[j][1];//个人bug数合计
				  columnTwo +=data[j][2];//交流数合计 
				  total3 += bugPercentNum;//bug占比合计
				  total4+=data[j][3]//总费解bug数合计
				  total5+=obscurePercentNum//费解占比合计
			  }
		  }
		  
	  } 
	  if(total3>0){
		  total3="100%"
	  }
	  if(total5>0){
		  total5=total5+"%"
	  }
	  tbody +="<tr style='background: #f0f1b3;'><td>合计</td><td>" + columnOne + "</td>" +
	          "<td>" + total3 + "</td><td>" + columnTwo + "</td>" +
	          		"<td>0.00</td><td>" + total4 + "</td><td>" + total5 + "</td></tr>";
	  document.getElementById("testerBugQualityTbody").innerHTML = tbody;
}
//保留2位小数
function getShortLengthNum(num){
	num = num*100;
	if((num.toString()).split(".").length > 1){
		var num01 =  (num.toString()).split(".")[0];//整数部分
		var num02 = (num.toString()).split(".")[1].substring(0,2);//保留2位小数
		var num03 = Number(num01 + "." +num02); 
		return num03;
	}else{
		return num;
	}
}
//# sourceURL=bugImpPhaseStat.js
