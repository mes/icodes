var seriesData = [];
var bugtype = [];
//当前版本号
var currentVersion = "";
var currentAge = "";
//
var chargeOwnerEcharts=null;
$(function(){
	$.parser.parse();
	//加载ehcarts
	var itemId="";
	itemId = $("#analyitemId").val(); 
	//期龄combobox初始化
	$("#age_select").xcombobox({
		onSelect: function(res) {
			currentAge = res.label; 
		},
		onLoadSuccess: function() {
				//如果有数据的话默认选中第一条数据
				$("#age_select").xcombobox('select', '1');
		},
	});
	//加载版本下拉
	$("#proVersion_owner").xcombobox({
		url: baseUrl + '/testTaskManager/testTaskManagerAction!loadVerSel.action?dto.taskId=' + itemId,
		loadFilter: function(data) {
			var obj = {};
			obj.keyObj = "";
			obj.valueObj = '所有'
			data.splice(0, 0, obj) //在数组0位置插入obj,不删除原来的元素
			return data;
		},
		onLoadSuccess: function(res) {
			var data = $("#proVersion_owner").xcombobox('getData'); //获取所有下拉框数据
			if (data.length > 0) {
				//如果有数据的话默认选中第一条数据
				$("#proVersion_owner").xcombobox('select', data[0].valueObj);
			}
		},

		valueField: 'keyObj',
		textField: 'valueObj',
		onSelect: function(res) {
			currentVersion = res.keyObj;
		}
	});
	//获取时间
	getBugDate(itemId);
	
	
});

function getBugDate(id){
	var url = baseUrl + "/singleTestTask/singleTestTaskAction!getBugDateLimit.action";
	$.post(
			url,
			{	"dto.singleTest.taskId":id
				},
			function(data){
				if(data.length>0){
					var dateArray = data.split("_");
					$("#startDate").datebox("setValue",dateArray[0]); 
					$("#endDate").datebox("setValue",dateArray[1]);
					loadChargeOwner(id,dateArray[0],dateArray[1]);
					
				}
				},'text');
}

function loadChargeOwner(itemId,startDate,endDate){
   
	var url = baseUrl + "/analysis/analysisAction!getChargeOwner.action";
	$.post(
			url,
			{	"analysisDto.taskId":itemId,
				"analysisDto.startDate":startDate,
				"analysisDto.endDate":endDate,
				"analysisDto.versionCode": currentVersion
				},
			function(data){
			   if(data.length>0){
				   $("#chargeOwnerDiv").css("display","block");
				   $("#chargeOwnerMask").css("display","none");
				   //构造echarts数据
				   constrEchartsData(data);
				   loadEcharts('','chargeOwnerEcharts');
				   loadTable();
			   }else{
				   $("#chargeOwnerDiv").css("display","none");
				   $("#chargeOwnerMask").css("display","block");
			   }
			},'json');
	    //加载其余的四个表格
	loadStateTable(itemId,startDate,endDate);
	loadLevelTable(itemId,startDate,endDate);
	loadTypeTable(itemId,startDate,endDate);
	loadAgeTable(itemId,startDate,endDate);
}
//构造echarts图表数据
function constrEchartsData(object){
	getType(object.concat());
	getSeriesData(object.concat());
}

//获取echarts的类别、日期

function getType(sourceData){
	bugtype = [];
	for(var i=0;i<sourceData.length;i++){
		if(bugtype.indexOf(sourceData[i][0]) == -1){
			bugtype.push(sourceData[i][0]);
		}
	}
}

//获取echarts的seriesData数据

function getSeriesData(sourData){
	seriesData = [];
	for(var i=0;i<bugtype.length;i++){
		var obj = {
			      name:bugtype[i]
		}
		for(var j=0;j<sourData.length;j++){
			if(sourData[j][0] == bugtype[i]){
				obj.value = sourData[j][1];
				break;
			}
		}
		seriesData.push(obj);
	}
}

function loadEcharts(title,echartsId){
	if(echartsId=='chargeOwnerEcharts'){
		if(chargeOwnerEcharts){
			chargeOwnerEcharts.clear();
		}else{
			chargeOwnerEcharts= echarts.init(document.getElementById(echartsId));
				
		}
		var myChart = chargeOwnerEcharts;
	}
	 // 指定图表的配置项和数据
       var	option = {
		    title : {
		        text: '缺陷处理人员bug分布情况',
		        x:'center'
		    },
		    tooltip : {
		        trigger: 'item',
		        formatter: "{a} <br/>{b} : {c} ({d}%)"
		    },
		    legend: {
		        type: 'scroll',
		        orient: 'vertical',
		        right: 0,
		        top: 20,
		        bottom: 20,
		        data: bugtype
		    },
		    series : [
		        {
		            name: '人员姓名',
		            type: 'pie',
		            radius : '55%',
		            center: ['50%', '50%'],
		            data: seriesData,
		            label: {
		                normal: {
		                    formatter: ' {b} {c}  {per|{d}%}',
		                    backgroundColor: '#eee',
		                    borderColor: '#aaa',
		                    borderWidth: 1,
		                    borderRadius: 4,
		                    // shadowBlur:3,
		                    // shadowOffsetX: 2,
		                    // shadowOffsetY: 2,
		                    // shadowColor: '#999',
		                    // padding: [0, 7],
		                    rich: {
		                        a: {
		                            color: '#999',
		                            lineHeight: 22,
		                            align: 'center'
		                        },
		                        // abg: {
		                        //     backgroundColor: '#333',
		                        //     width: '100%',
		                        //     align: 'right',
		                        //     height: 22,
		                        //     borderRadius: [4, 4, 0, 0]
		                        // },
		                        hr: {
		                            borderColor: '#aaa',
		                            width: '100%',
		                            borderWidth: 0.5,
		                            height: 0
		                        },
		                        b: {
		                            fontSize: 16,
		                            lineHeight: 33
		                        },
		                        per: {
		                            color: '#eee',
		                            backgroundColor: '#334455',
		                            padding: [2, 4],
		                            borderRadius: 2
		                        }
		                    }
		                }
		            },
		            itemStyle: {
		                emphasis: {
		                    shadowBlur: 10,
		                    shadowOffsetX: 0,
		                    shadowColor: 'rgba(0, 0, 0, 0.5)'
		                }
		            }
		        }
		    ]
		};
    // 使用刚指定的配置项和数据显示图表。
    myChart.setOption(option);
}

//加载表格
function loadTable(){
	  var tbody = "";
	  var count = 0;
	  for(var i=0;i<seriesData.length;i++){
		  if(null == seriesData[i].value){
			  seriesData[i].value = "-";
		  }else{
			  count +=  seriesData[i].value; 
		  }
		  tbody += "<tr><td>"+ seriesData[i].name + "</td>" +
		            "<td>" + seriesData[i].value + "</td></tr>";
	  } 
	  tbody +="<tr style='background: #f0f1b3;'><td>BUG总数</td><td>" + count + "</td></tr>";
	  $("#chargeOwnerTbody").html(tbody);
}

//查看报表
document.getElementById("viewReport").addEventListener('click',function(){
	var itemId = "",startDate="",endDate="";
	itemId = $("#analyitemId").val(); 
	var startDate = $("#startDate").datebox('getValue'); 
	var endDate = $("#endDate").datebox('getValue'); 
	if(null == startDate || startDate == ""){
		$.xalert("请选择开始日期");
		return;
	}
	
	if(null == endDate || endDate == ""){
		$.xalert("请选择结束日期日期");
		return;
	}
	
	loadChargeOwner(itemId,startDate,endDate);
});

//重置输入框
document.getElementById("resetInp").addEventListener('click',function(){
	$('#startDate').datebox('setValue', '');	
	$('#endDate').datebox('setValue', '');
	$('#proVersion_owner').xcombobox('setValue', '');
	$('#age_select').xcombobox('setValue', '1');
});
//加载bug状态按人分布表格
function loadStateTable(itemId,startDate,endDate){
	var url = baseUrl + "/analysis/analysisAction!getChargeOwnerAndState.action";
	$.post(
			url,
			{	"analysisDto.taskId":itemId,
				"analysisDto.startDate":startDate,
				"analysisDto.endDate":endDate,
				"analysisDto.versionCode": currentVersion
				},
			function(data){
				console.log(data);
			   if(data.length>0){
				   var html = "";
				   for(var i=0;i<data.length;i++){
					   var state = ""
					   if(data[i].current_state){
						   state = data[i].current_state
					   }else{
						   state = data[i].CURRENT_STATE
					   }
					   html += "<tr><td style='text-align:center;'>"+ data[i].name+ "</td>" +
				       "<td style='text-align:center;'>" + state + "</td>" + 
			           "<td style='text-align:center;'>" + data[i].bugCount + "</td></tr>" ;
				   }
				   $("#stateTableTbody").html(html); 
			   } 
			},'json');
}
//加载bug状态按人分布表格
function loadLevelTable(itemId,startDate,endDate){
	var url = baseUrl + "/analysis/analysisAction!getChargeOwnerAndLevel.action";
	$.post(
			url,
			{	"analysisDto.taskId":itemId,
				"analysisDto.startDate":startDate,
				"analysisDto.endDate":endDate,
				"analysisDto.versionCode": currentVersion
				},
			function(data){
				console.log(data);
			   if(data.length>0){
				   var html = "";
				   for(var i=0;i<data.length;i++){ 
					   html += "<tr><td style='text-align:center;'>"+ data[i].NAME+ "</td>" +
				       "<td style='text-align:center;'>" + data[i].bug_level + "</td>" + 
			           "<td style='text-align:center;'>" + data[i].bugCount + "</td></tr>" ;
				   }
				   $("#levelTableTbody").html(html); 
			   } 
			},'json');
}
//加载bug状态按人分布表格
function loadTypeTable(itemId,startDate,endDate){
	var url = baseUrl + "/analysis/analysisAction!getChargeOwnerAndType.action";
	$.post(
			url,
			{	"analysisDto.taskId":itemId,
				"analysisDto.startDate":startDate,
				"analysisDto.endDate":endDate,
				"analysisDto.versionCode": currentVersion
				},
			function(data){
				console.log(data);
			   if(data.length>0){
				   var html = "";
				   for(var i=0;i<data.length;i++){ 
					   html += "<tr><td style='text-align:center;'>"+ data[i].name+ "</td>" +
				       "<td style='text-align:center;'>" + data[i].bug_type + "</td>" + 
			           "<td style='text-align:center;'>" + data[i].bugCount + "</td></tr>" ;
				   }
				   $("#typeTableTbody").html(html); 
			   } 
			},'json');
}
//加载bug状态按人分布表格
function loadAgeTable(itemId,startDate,endDate){
	var url = baseUrl + "/analysis/analysisAction!getChargeOwnerAndAge.action";
	$.post(
			url,
			{	"analysisDto.taskId":itemId,
				"analysisDto.startDate":startDate,
				"analysisDto.endDate":endDate,
				"analysisDto.versionCode": currentVersion,
				"analysisDto.dayOrWeek": currentAge,
				},
			function(data){
				console.log(data);
			   if(data.length>0){
				   var html = "";
				   for(var i=0;i<data.length;i++){ 
					   html += "<tr><td style='text-align:center;'>"+ data[i].ownerName+ "</td>" +
				       "<td style='text-align:center;'>" + data[i].age + "</td>" + 
			           "<td style='text-align:center;'>" + data[i].current_state + "</td>" +
			           		"<td style='text-align:center;'>" + data[i].bugCount + "</td></tr>" ;
				   }
				   $("#ageTableTbody").html(html); 
			   } 
			},'json');
}
//@ sourceURL=chargeOwner.js
