// dom缓存，提升页面访问性能
var windowHeight;
var objs = {
	$body: $("body"),
	$switchBtn: $("#switch_btn"),
	$loginForm: $("#loginForm"),
	$loginName: $("#loginlName"),
	$loginPwd: $("#loginPwd"),
	$remember: $("#remember"),
	$ldapLogin: $("#ldapLogin")
};
/*存放登录按钮是否是disabled*/
var disableButton = false;
var logOut = false;
$(function() {
	//var height = $(".login-form").height();
	//console.log(height);
	//获取loginBu距离可视区域底部的高度
	getLoginDistance();
	objs.$switchBtn.on('click', function() {
		$(this).toggleClass("collapse");
		objs.$body.toggleClass("collapse");
	});
	if (GetQueryString("logOut") != null) {
		logOut = GetQueryString("logOut");
	}
	if (GetQueryString("mailBugId") != null) {
		localStorage.mailBugId = GetQueryString("mailBugId");
	}
	if (GetQueryString("taskId") != null) {
		localStorage.taskId = GetQueryString("taskId");
	}
	if (GetQueryString("isMyself") != null) {
		localStorage.isMyself = GetQueryString("isMyself");
	}
	// 记住我
	rememberMe();

	// 单击回车键登录
	$(document).keypress(function(e) {
		if (e.keyCode == 13) {
			if (!disableButton) {
				login();
			}
		}
	});
	//判断浏览器是否为火狐或者chrome
	if (navigator.userAgent.indexOf("Firefox") == -1 && navigator.userAgent.indexOf("Chrome") == -1) {
		alert("请使用火狐浏览器或者chrome浏览器登录");
	}
	windowHeight = window.screen.height;
	if (windowHeight > 899) {
		$(".login").css('padding-top', windowHeight * 0.13 + 'px');
	}

});

function GetQueryString(name) {
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
	var r = window.location.search.substr(1).match(reg);
	if (r != null) return (r[2]);
	return null;
}



// 记住用户名和密码
function rememberMe() {
	if ("undefined" != typeof localStorage.checked && "true" == localStorage.checked) {
		objs.$loginName.on('input propertychange', function() {
			localStorage.loginName = objs.$loginName.val();
		});

		objs.$loginPwd.on('input propertychange', function() {
			localStorage.password = objs.$loginPwd.val();
		});
		if ("undefined" != typeof localStorage.loginName) {
			objs.$loginName.val(localStorage.loginName);
		}
		//密码解密
		try {
			var pword = Security.enc.AES.cbcDecrypt(localStorage.password, "670b14728ad9902aecba32e22fa4f6bd");
		} catch (err) {
			localStorage.password = Security.enc.AES.cbcEncrypt(localStorage.password, "670b14728ad9902aecba32e22fa4f6bd");
			var pword = Security.enc.AES.cbcDecrypt(localStorage.password, "670b14728ad9902aecba32e22fa4f6bd");
			//objs.$loginPwd.val(pword);
		}

		objs.$loginPwd.val(pword);
		objs.$remember.attr('checked', 'checked');
		//判断是否需要自动登录
		if (!logOut) {
			login();
		} else {
			if ("undefined" != typeof localStorage.loginName && localStorage.loginName != "") {
				$("#holder01").text("");
			}
			if ("undefined" != typeof localStorage.password && localStorage.password != "") {
				$("#holder02").text("");
			}
		}

	} else {
		sessionStorage.removeItem("loginName");
		sessionStorage.removeItem("password");
	}

	/*objs.$remember.on('click', function() {
		if (objs.$remember.is(':checked')) {
			localStorage.checked = 'true';
			localStorage.loginName = objs.$loginName.val();
			//密码加密
			localStorage.password = Security.enc.AES.cbcEncrypt(objs.$loginPwd.val(), "670b14728ad9902aecba32e22fa4f6bd");
			//localStorage.password = objs.$loginPwd.val();
		} else {
			localStorage.checked = 'false';
			sessionStorage.removeItem("loginName");
			sessionStorage.removeItem("password");
		}
	});*/
}

// 登录
function login() {
	/*将登陆按钮设置成disabled*/
	$("#loginBu").attr("disabled", "disabled");
	disableButton = true;
	var loginUrl = "";
	var ldapFlg = objs.$ldapLogin.is(':checked');
	if(ldapFlg){
		loginUrl = baseUrl + '/ldapVerification/ldapVerificationAction!ldapUserAuthentication.action';
	}else{
		loginUrl = baseUrl + '/userManager/userManagerAction!login.action';
	}
	
	$.ajax({
		type: 'POST',
		url: loginUrl,
//		url: baseUrl + '/userManager/userManagerAction!login.action',
//		url: baseUrl + '/ldapVerification/ldapVerificationAction!ldapUserAuthentication.action',
		data: /*objs.$loginForm.serialize()*/ {
			'dto.user.loginName': $("#loginlName").val().trim(),
			'dto.user.password': $("#loginPwd").val().trim()
//			'ldapVerificationDto.user.loginName': "user1",
//			'ldapVerificationDto.user.password': "12345"
		},
		dataType: 'json'
	}).done(function(data) {

		if ("success" == data.loginItest) {
			if (objs.$remember.is(':checked')) {
				localStorage.checked = 'true';
				//加密密码
				localStorage.password = Security.enc.AES.cbcEncrypt(objs.$loginPwd.val(), "670b14728ad9902aecba32e22fa4f6bd");
				localStorage.loginName = objs.$loginName.val();
			} else {
				localStorage.checked = 'false';
				sessionStorage.removeItem("loginName");
				sessionStorage.removeItem("password");
			}
			objs.$loginForm.find('button').text('登录中...');
			//window.location.href = baseUrl + "/" + data.HomeUrl;
			window.location.href = baseUrl + "/main.htm";

		} else {
			$("#login-error").show();
			//$.xnotify("用户名或密码错误！", {type:'warning'});
			$("#loginBu").removeAttr("disabled");
			disableButton = false;
		}
	}).fail(function(xhr, textStatus, errorThrown) {
		$("#login-error").html("");
		if(ldapFlg){
			$("#login-error").html("<div class=\"login-error-msg\">服务连接不上，请检查LDAP服务配置信息！</div>");
		}else{
			$("#login-error").html("<div class=\"login-error-msg\">登录失败！</div>");
		}
		$("#login-error").show();
		/*$.xnotify("登录失败！", {
			type: 'danger'
		});*/
		
		$("#loginBu").removeAttr("disabled");
		disableButton = false;
	});

	/*$.post(
		baseUrl + '/userManager/userManagerAction!login.action',
		objs.$loginForm.serialize(),
		function(data) {
			if ("success" == data.loginMYPM) {
				window.location.href = baseUrl + "/" + data.HomeUrl;
			} else {
				console.log("登录失败！");
			}
		},
		'json'
	);*/
}
//获取按钮距离底部的距离
function getLoginDistance(){
	//浏览器自身的高
/*var windowHeight  = window.screen.height;
   //可见区域的高度
	var clientHeight = document.body.clientHeight;
	//被卷去的高度
	var scrollHeight = document.body.scrollTop;
	//元素距离顶部的高度
	var offsetTop = document.getElementById("loginBu").offsetTop;
	//元素自身的高度
	var eleHeight = document.getElementById("loginBu").offsetHeight;
	//元素距离底部的高度
	var bottomDis = clientHeight+scrollHeight-(offsetTop+eleHeight);*/
	//获取操作系统
	var systemName = getOS();
	var zoom = detectZoom();
	if(systemName=="Win10"){
		if(zoom==100){
			window.localStorage.setItem('screenZoom',"win10100");
		}else{
			window.localStorage.setItem('screenZoom',zoom);
		}
		
	}else{
		window.localStorage.setItem('screenZoom',100);
	}
	//取值 Number(window.localStorage.getItem('screenZoom'));
}
function getOS() {
    var sUserAgent = navigator.userAgent;
    var isWin = (navigator.platform == "Win32") || (navigator.platform == "Windows");
    var isMac = (navigator.platform == "Mac68K") || (navigator.platform == "MacPPC") || (navigator.platform == "Macintosh") || (navigator.platform == "MacIntel");
    if (isMac) return "Mac";
    var isUnix = (navigator.platform == "X11") && !isWin && !isMac;
    if (isUnix) return "Unix";
    var isLinux = (String(navigator.platform).indexOf("Linux") > -1);
    if (isLinux) return "Linux";
    if (isWin) {
        var isWin2K = sUserAgent.indexOf("Windows NT 5.0") > -1 || sUserAgent.indexOf("Windows 2000") > -1;
        if (isWin2K) return "Win2000";
        var isWinXP = sUserAgent.indexOf("Windows NT 5.1") > -1 || sUserAgent.indexOf("Windows XP") > -1;
        if (isWinXP) return "WinXP";
        var isWin2003 = sUserAgent.indexOf("Windows NT 5.2") > -1 || sUserAgent.indexOf("Windows 2003") > -1;
        if (isWin2003) return "Win2003";
        var isWinVista= sUserAgent.indexOf("Windows NT 6.0") > -1 || sUserAgent.indexOf("Windows Vista") > -1;
        if (isWinVista) return "WinVista";
        var isWin7 = sUserAgent.indexOf("Windows NT 6.1") > -1 || sUserAgent.indexOf("Windows 7") > -1;
        if (isWin7) return "Win7";
        var isWin10 = sUserAgent.indexOf("Windows NT 10") > -1 || sUserAgent.indexOf("Windows 10") > -1;
        if (isWin10) return "Win10";
    }
    return "other";
}
function detectZoom() {
    var ratio = 0,
        screen = window.screen,
        ua = navigator.userAgent.toLowerCase();

    if (window.devicePixelRatio !== undefined) {
        ratio = window.devicePixelRatio;
    }
    else if (~ua.indexOf('msie')) {
        if (screen.deviceXDPI && screen.logicalXDPI) {
            ratio = screen.deviceXDPI / screen.logicalXDPI;
        }
    }
    else if (window.outerWidth !== undefined && window.innerWidth !== undefined) {
        ratio = window.outerWidth / window.innerWidth;
    }

    if (ratio) {
        ratio = Math.round(ratio * 100);
    }
    return ratio;
}