var objs = {
	$ldapServerConfigList:$("#ldapServerConfigList"),
	$ldapConfigInfoWin:$("#ldapConfigInfoWin"),
	$ldapConfigInfoForm:$("#ldapConfigInfoForm"),
	$enableLd:$("#enableLd")
};

$(function(){
	$.parser.parse();
	
	//ldap服务配置信息
	ldapServerConfigInfo();
	
	//回车事件
	document.getElementById('ldapServerConfigTool').onkeydown=function(event) {
		var e = event || window.event || arguments.callee.caller.arguments[0];
		if(e && e.keyCode==13){ // 按 Esc
			searchLdapConfigInfo();
		}
	};
	$('input[name="ldapConfigInfoDto.ldapConfigInfo.baseDn"][type="hidden"]').prev().attr("placeholder","例如：cn=Manager,dc=itest,dc=com");
	
//	objs.$enableLd.xcombobox('select', '0');
});

function searchLdapConfigInfo(){
	objs.$ldapServerConfigList.xdatagrid('load', {
	    "ldapConfigInfoDto.ldapConfigInfo.baseDn": $("#ldapServerConfigQuery").val()
	});
}

//重置
function ldapServerConfigReset(){
	$("#ldapServerConfigQuery").val("");
	searchLdapConfigInfo();
}

//ldap服务配置信息
function ldapServerConfigInfo(){
	objs.$ldapServerConfigList.xdatagrid({
		url: baseUrl + '/ldapConfigInfo/ldapConfigInfoAction!ldapServerConfigInfo.action',
		method: 'post',
		height: mainObjs.tableHeight, 
//		queryParams:{
//			'dto.iterationList.iterationBagName':iterationBagName,
//			'dto.iterationList.createPerson':createPerson,
//			'dto.iterationList.status':"0",
//			'dto.iterationList.associationProject':proAllnameSel
//		},
		striped:false,
		fitColumns: true,
		rownumbers: true,
		singleSelect: true,
		checkOnSelect: true,
		selectOnCheck: true,
		pagination: true,
		
		pageNumber: 1,
		pageSize: 10,
		emptyMsg:"暂无数据",
		layout:['list','first','prev','manual','next','last','refresh','info'],
		columns:[[
	        {field:'checkId',title:'选择',checkbox:true,align:'center'},
		    {field:'serverIp',title:'IP地址',width:'15%',align:'center',halign:'center'},
		    {field:'port',title:'端口',width:'12%',align:'center',halign:'center'},
			{field:'baseDn',title:'BASEDN路径',width:'20%',align:'center',halign:'center'},
			{field:'bindPassword',title:'密码',width:'10%',align:'center',halign:'center'},
			{field:'createTime',title:'创建时间',width:'11%',align:'center',halign:'center',formatter:createTime},
			/*{field:'updateTime',title:'更新时间',width:'15%',align:'center',halign:'center'},*/
			{field:'note',title:'备注',width:'20%',align:'center'},
			{field:'enable',title:'操作',width:'10%',align:'center',formatter:operatFormat}
		]],
		onLoadSuccess : function (data) {
			
		},
		onDblClickRow: function (rowIndex, rowData){  
			
		},
		onSelect: function(index, data) {  
			
		},
		onCheck: function(index, row) { 
			
		},
	});
}

//操作
function operatFormat(value,row,index){
	if(value == "0"){
		return "<a onclick=\"disableLdap('disableLdap')\" id=\"disableLdap\" style=\"cursor: pointer;text-decoration: underline;\">禁用</a>";
	} else{
		return "<a onclick=\"enableLdap('enableLdap')\" id=\"enableLdap\" style=\"cursor: pointer;text-decoration: underline;\">启用</a>";
	}
}

//禁用
function disableLdap(ele){
	var keyId = objs.$ldapServerConfigList.xdatagrid('getSelections')[0].id;
	var enableFlg = findLdapIsEnableRecord();
	if(enableFlg == "failed"){
		editLdapEnable(keyId,"1");
	}else{
		$.xalert({title:'提示',msg:"已有一条启用记录!"});
		 return;
	}
}

function findLdapIsEnableRecord(){
	var flag = "";
	$.post(
			baseUrl + "/ldapConfigInfo/ldapConfigInfoAction!findLdapIsEnableRecord.action",null,
			function(dataObj) {
				flag = dataObj;
			},"text");
	return flag;
}

function editLdapEnable(id,param){
	$.post(
			baseUrl + "/ldapConfigInfo/ldapConfigInfoAction!editLdapStatus.action",
			{
				"ldapConfigInfoDto.ldapConfigInfo.id":id,
				"ldapConfigInfoDto.ldapConfigInfo.enable":param
			},
			function(dataObj) {
				if(dataObj.indexOf("success") >=0 ){
					  objs.$ldapServerConfigList.xdatagrid('reload');
				  }
			},"text"
		);

}

//启用
function enableLdap(eles){
	var keyIds = objs.$ldapServerConfigList.xdatagrid('getSelections')[0].id;
	editLdapEnable(keyIds,"0");
}

function createTime(value,row,index){
	if(value==null || value==""){
		return "--";
	}else{
		return row.createTime.split(" ")[0];
	}
}

function ldapServerConfigAdd(){
	objs.$ldapConfigInfoForm.xform('clear');
	objs.$ldapConfigInfoWin.xwindow('setTitle','新建LDAP服务配置').xwindow('open');
	$("#ldapId").val("");
	$("#ldapEnable").val("0");
}

function saveLdapConfigInfo(){
	var ldapNote = $("#ldapNote").xtextbox("getValue");
	var ldapPort = $("#ldapPort").xtextbox("getValue");
	var ldapBaseDn = $("#ldapBaseDn").xtextbox("getValue");
	var ldapServerIp = $("#ldapServerIp").xtextbox("getValue");
	var bindPassword = $("#bindPassword").xtextbox("getValue");
	
	if(!ldapServerIp){
		 $.xalert({title:'提示',msg:"请填写serverIp!"});
		 return;
	}
	
	if(!ldapPort){
		 $.xalert({title:'提示',msg:"请填写端口!"});
		 return;
	}
	
	if(!ldapBaseDn){
		 $.xalert({title:'提示',msg:"请填写baseDn绝对路径!"});
		 return;
	}
	
	if(!bindPassword){
		 $.xalert({title:'提示',msg:"请填写密码!"});
		 return;
	}
	
	var url=baseUrl+"/ldapConfigInfo/ldapConfigInfoAction!saveOrUpdateLdapConfigInfo.action";
	$.ajax({
		  url: url,
		  cache: false,
		  async: false,
		  type: "POST",
//		  dataType:"json",
		  dataType:"text",
		  data: {
			  "ldapConfigInfoDto.ldapConfigInfo.id":$("#ldapId").val(),
			  "ldapConfigInfoDto.ldapConfigInfo.serverIp":ldapServerIp,
			  "ldapConfigInfoDto.ldapConfigInfo.port":ldapPort,
			  "ldapConfigInfoDto.ldapConfigInfo.baseDn":ldapBaseDn,
			  "ldapConfigInfoDto.ldapConfigInfo.bindPassword":bindPassword,
			  "ldapConfigInfoDto.ldapConfigInfo.createPerson":$("#accountId").text(),
			  "ldapConfigInfoDto.ldapConfigInfo.note":ldapNote,
			  "ldapConfigInfoDto.ldapConfigInfo.enable":$("#ldapEnable").val()
		  },
		  success: function(data){
			  if(data.indexOf("success") >=0 ){
				  cancleConfigInfo();
				  objs.$ldapServerConfigList.xdatagrid('reload');
			  }
		  }
		});
	
}

//编辑
function ldapServerConfigEdit(){
	var ldapConfigInfo = objs.$ldapServerConfigList.xdatagrid('getSelections');
	if(ldapConfigInfo.length <= 0){
		$.xalert({title:'提示',msg:'请选择一条记录！'});
		return;
	}
	objs.$ldapConfigInfoForm.xform('clear');
	objs.$ldapConfigInfoWin.xwindow('setTitle','修改LDAP服务配置').xwindow('open');
	$.post(
			baseUrl + "/ldapConfigInfo/ldapConfigInfoAction!searchLdapConfigInfo.action",
			{"ldapConfigInfoDto.ldapConfigInfo.id": ldapConfigInfo[0].id},
			function(dataObj) {
				if(dataObj != null){
					$("#ldapNote").xtextbox("setValue",dataObj.note);
					$("#ldapPort").xtextbox("setValue",dataObj.port);
					$("#ldapBaseDn").xtextbox("setValue",dataObj.baseDn);
					$("#ldapServerIp").xtextbox("setValue",dataObj.serverIp);
					$("#bindPassword").xtextbox("setValue",dataObj.bindPassword);
					$("#ldapEnable").val(dataObj.enable);
					$("#ldapId").val(dataObj.id);
				}
			},"json"
		);
}

//删除
function ldapServerConfigDel(){
	var ldapConfigInfo = objs.$ldapServerConfigList.xdatagrid('getSelections');
	if(ldapConfigInfo.length <= 0){
		$.xalert({title:'提示',msg:'请选择一条记录！'});
		return;
	}else{
		if(ldapConfigInfo[0].enable == 1){
			$.xalert({title:'提示',msg:'启用的服务配置信息不能删除！'});
			return;
		}
	}
	
	$.post(
			baseUrl + "/ldapConfigInfo/ldapConfigInfoAction!deleteLdapConfigInfo.action",
			{"ldapConfigInfoDto.ldapConfigInfo.id": ldapConfigInfo[0].id},
			function(dataObj) {
				if(dataObj.indexOf("success") >=0){
					objs.$ldapServerConfigList.xdatagrid('reload');
				}
			},"text"
		);
}

//清除关闭模态窗
function cancleConfigInfo(){
	objs.$ldapConfigInfoForm.xform('clear');
	objs.$ldapConfigInfoWin.xwindow('close');
}


//# sourceURL=ldapServerConfig.js