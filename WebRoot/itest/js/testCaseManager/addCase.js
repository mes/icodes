var fileInfos = new Array();
var iterationFlag = "";
$(function() {
	$.parser.parse();
	loadCaseCategory();
	loadCaseYXJ();
	loadExecVersion();
	var taskId = getQueryParam('taskId');
	var moudleId = getQueryParam('moudleId');
	iterationFlag = getQueryParam('flag');
	addWindow(taskId, moudleId);
	//加载用户历史标签
	loadUserHistoryTags();
	/*$('#tagsField').tagit({
        availableTags: ['石油','环保','技能','可以','你好','节能'],
        // This will make Tag-it submit a single form value, as a comma-delimited field.
        singleField: true,
        singleFieldNode: $('#tagsValueField')
    });*/
	$('.isSelected input[type=checkbox]').on('click', function(){
 	   var selectedItems = $(this).parents('.dropDown-menus').prevAll('.selectedContent').val().split(' ');
 	   var thisItem = $(this).parent().prev().text();
 	   var isExisted = 0;
 	   var isChecked = $(this).is(':checked');
 	   if(isChecked){
 	    selectedItems.map(function(item, index){
 	     if(item === thisItem){
 	      isExisted++
 	     }
 	    });
 	    if(!isExisted){
 	     selectedItems.push(thisItem)
 	    }
 	   }
 	   else{
 	    selectedItems.map(function(item, index){
 	     if(item === thisItem){
 	      selectedItems.splice(index, 1);
 	     }
 	    });
 	   }
 	   $(this).parents('.dropDown-menus').prevAll('.selectedContent').val(selectedItems.join(' '));
 	  })
 	  $('.confirmSelect').on('click', function(){
 	   $(this).parents('.dropDown-menus').addClass('dsn');
 	  })
 	  $('.dropDown-toggle').on('click', function(){
 	   $(this).next().toggleClass('dsn')
 	  });

});
//获取页面url参数
function getQueryParam(name) {
	var obj = $('#addOrEditWindown').xwindow('options');
	var queryParams = obj["queryParams"];
	return queryParams[name];
}

//加载用户历史标签
function loadUserHistoryTags(){
	$.post(baseUrl + "/userTag/userTagAction!getUserTagsByUserId.action",{
		'dto.userTag.userId':$("#accountId").text()
	},function(data){
		if(data.length > 0){
			var tagHtml = "";
			for(var i=0;i<data.length;i++){
				tagHtml = tagHtml + '<li class="items">'
			    +'<span class="isSelectedText">'+data[i].tagName+'</span>'
			    +'<span class="isSelected"><input type="checkbox"></span>'
			    +'</li>';
			}
			tagHtml = tagHtml + '<li style="text-align: right">'
			+'<button type="button" class="confirmSelect l-btn btn btn-primary btn-default btn-xs exui-linkbutton">确定</button>'
			+'</li>';
			$("#dropDown-menus").html(tagHtml);
		}
	},'json');
}

//增加
function addWindow(taskId, moudleId) {
	$("#addOrEditCaseForm").xform('clear');
	$("#caseWeight").xnumberbox("setValue", 1);
	$("#task_Id").val(taskId);
	$("#moduleId").val(moudleId);
	caseUpload();
	$("#operDataRichText").next("span").css("margin-bottom","3px");
}


//提交保存新增或修改
function submitForm(submitId) {
	var valid = $("#addOrEditCaseForm").xform('validate');
	if (!valid) {
		return;
	}
//	if (submitId == "2") {
//		if ($("#exeVerId").xcombobox("getValue") == "") {
//			$.xalert({
//				title: '提示',
//				msg: '请选择版本！'
//			});
//			return;
//		}
//	}
	
	var filesCount = $('#caseUploadFileId').fileinput('getFilesCount');
	if (filesCount > 0) {
		$.xalert({
			title: '提示',
			msg: '您还有选中的文件没上传，请先上传!'
		});
		return;
	}

	var fileParam = {
		'dto.fileInfos': JSON.stringify(fileInfos)
	};
	var dataParams = Object.assign($("#addOrEditCaseForm").xserialize(), fileParam);
	$('#submit_' + submitId).linkbutton('disable');
	var testCaseId = $("#testCaseId").val();
	var url;
	if (testCaseId == "") {
		url = baseUrl + "/caseManager/caseManagerAction!addCase.action";
	} else {
		url = baseUrl + "/caseManager/caseManagerAction!upCase.action"
	}
	$.post(
		url,
		//		$("#addOrEditWindown").xserialize(),
		dataParams,
		function(data) {
			if (data == "success") {
				if (iterationFlag != "iterationPage") {
					getCaseList(currNodeId);
				}
				$('#submit_1').linkbutton('enable');
				$('#submit_2').linkbutton('enable');
				if (testCaseId == "") {
					if (submitId == "2") {
						$.xalert({
							title: '提示',
							msg: '保存成功！',
							okFn: function() {
								$('#caseUploadFileId').fileinput('clear');
//								$("#addOrEditCaseForm").xform('clear');
								$("#testCaseDes").xtextbox("setValue", "");
								$("#prefixCondition").xtextbox("setValue", "");
								$("#operDataRichText").xtextbox("setValue", "");
								$("#expResult").xtextbox("setValue", "");
								$("#testCaseRemark").xtextbox("setValue", "");
							}
						});
					} else {
						$.xalert({
							title: '提示',
							msg: '保存成功！',
							okFn: function() {
								closeCaseWin();
							}
						});
						
					}
					
				} else {
					if (submitId == "2") {
						$.xalert({
							title: '提示',
							msg: '保存成功！'
						});
					} else {
						$.xalert({
							title: '提示',
							msg: '修改成功！'
						});
					}
				}
			} else {
				$('#submit_' + submitId).linkbutton('enable');
				$.xalert({
					title: '提示',
					msg: '系统错误！'
				});
			}
		}, "text");
}

function caseUpload() {
	$("#caseUploadFileId").fileinput('reset');
	$("#caseUploadFileId").fileinput('destroy');
	$("#caseUploadFileId").fileinput({
		theme: 'fa',
		language: 'zh',
		uploadUrl: baseUrl + "/uploader?type=case", // you must set a valid URL here else you will get an error
		deleteUrl: '',
		//		allowedFileExtensions: ['jpg', 'png', 'gif','bmp','ppt','pptx','txt','pdf','doc','docx','object'],
		showPreview: true,
		overwriteInitial: false,
		uploadAsync: false,
		autoReplace: true,
		//		showUploadedThumbs:false,
		maxFileSize: 6000,
		maxFileCount: 5,
		imageMaxWidth: 200,
		imageMaxHeight: 100,
		enctype: 'multipart/form-data',
		showRemove: false,
		showClose: false,
		//		showUpload:false,
		allowedPreviewTypes: ['image', 'html', 'text', 'video', 'pdf', 'flash', 'object'],
		dropZoneEnabled: false,
		fileActionSettings: {
			showUpload: false,
			showRemove: true,
			//			url: baseUrl + "/uploader?type=del",// 删除url 
		},
		slugCallback: function(filename) {
			return filename.replace('(', '_').replace(']', '_');
		}
	}).on("filebatchselected", function(event, files) {
		//		if(files!=null && files.length>0){
		//			$(this).fileinput("upload");
		//		}
	}).on('filebatchuploadsuccess', function(event, data, previewId, index) {
		fileInfos = fileInfos.concat(data.response);
		setTimeout(function() {
			$(".kv-upload-progress").hide();
		}, 2000);
	}).on('filebatchuploaderror', function(event, data, previewId, index) {}).on('fileloaded', function(event, data, previewId, index) {
		var filesFrames = $('#caseUploadFileId').fileinput('getFrames');
		if (filesFrames.length > 5) {
			$.xalert({
				title: '提示',
				msg: '最多上传5个文件！'
			});
			$("#" + previewId).remove();
			return;
		}


	}).on("fileuploaded", function(event, data, previewId, index) {
		//上传成功后处理方法
	});
	$(".file-caption-name").attr('placeholder', '点击后粘贴截屏或选择文件...');
	$('.file-caption-name')[0].onkeydown=function(event) {
		var e = event || window.event || arguments.callee.caller.arguments[0]; 
	    if (e && e.keyCode == 13) {//keyCode=13是回车键；数字不同代表监听的按键不同
	    	return false;
	    }
	};
}

function loadCaseCategory() {
	$.post(
		baseUrl + "/testBaseSet/testBaseSetAction!loadTestBaseSetList.action", {
			"page": 1,
			"rows": 80,
			"dto.subName": "用例类型",
			"dto.flag": "1"
		},
		function(dat) {
			if (dat != null) {
				$(".caseTypeId").xcombobox({
					data: dat.rows
				});
			} else {
				/*$.xnotify("系统错误！", {type:'warning'});*/
				$.xalert({
					title: '提示',
					msg: '系统错误！'
				});
			}
		}, "json");
}

//加载优先级列表
function loadCaseYXJ() {
	$.post(
		baseUrl + "/testBaseSet/testBaseSetAction!loadTestBaseSetList.action", {
			"page": 1,
			"rows": 80,
			"dto.subName": "用例优先级",
			"dto.flag": "1"
		},
		function(dat) {
			if (dat != null) {
				$(".priId").xcombobox({
					data: dat.rows
				});
			} else {
				/*$.xnotify("系统错误！", {type:'warning'});*/
				$.xalert({
					title: '提示',
					msg: '系统错误！'
				});
			}
		}, "json");
}

//取消
function closeCaseWin() {
	$("#addOrEditCaseForm").xform('clear');
	$("#caseWeight").numberbox("setValue", 2);
	$("#addOrEditWindown").xwindow('close');
}
//加载基线,不能注提，在BUG关联用例时，增加用例时，要调这个方法
function loadExecVersion() {
	$("#baseline").xcombobox({
		url: baseUrl + '/testTaskManager/testTaskManagerAction!loadVerSel.action',
		valueField: 'keyObj',
		textField: 'valueObj',
		onSelect: function(rec) {
		}
	});
}

function showOrHideTagTr(){
	$("#tagTr").toggle();
}
//# sourceURL=addCase.js